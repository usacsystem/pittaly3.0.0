class CreatePreparations < ActiveRecord::Migration
  def change
    create_table :preparations do |t|
      t.string        :type
      t.references    :category
      t.string        :hint,          default: ""
      t.timestamps
    end

    add_index :preparations, [:type, :category_id],
      name:     "primary_index",
      unique:   false
  end
end
