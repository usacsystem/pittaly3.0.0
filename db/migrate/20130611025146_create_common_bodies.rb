class CreateCommonBodies < ActiveRecord::Migration
  def change
    create_table :common_bodies do |t|
      t.references  :common_head
      t.string      :skucd,   null: false,  default: ""
      t.timestamps
    end

    add_index :common_bodies, [:common_head_id, :skucd],
      name:     'primary_index',
      unique:   true
  end
end
