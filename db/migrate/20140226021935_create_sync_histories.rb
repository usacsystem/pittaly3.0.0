class CreateSyncHistories < ActiveRecord::Migration
  def change
    create_table :sync_histories do |t|
      t.references  :user
      t.integer     :cd
      t.timestamps
    end

    add_index :sync_histories, [:user_id, :cd],
      name:     'primary_index',
      unique:   true
  end
end
