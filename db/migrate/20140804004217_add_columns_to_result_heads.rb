class AddColumnsToResultHeads < ActiveRecord::Migration
  def up
    # 文字予備
    add_column :result_heads, :string1, :string, default: "", after: :staffcd
    add_column :result_heads, :string2, :string, default: "", after: :string1
    add_column :result_heads, :string3, :string, default: "", after: :string2
    add_column :result_heads, :string4, :string, default: "", after: :string3
    add_column :result_heads, :string5, :string, default: "", after: :string4
    # 数字予備
    add_column :result_heads, :decimal1, :decimal, default: 0, precision: 13, scale: 3, after: :string5
    add_column :result_heads, :decimal2, :decimal, default: 0, precision: 13, scale: 3, after: :decimal1
    add_column :result_heads, :decimal3, :decimal, default: 0, precision: 13, scale: 3, after: :decimal2
    add_column :result_heads, :decimal4, :decimal, default: 0, precision: 13, scale: 3, after: :decimal3
    add_column :result_heads, :decimal5, :decimal, default: 0, precision: 13, scale: 3, after: :decimal4
    # 日付
    add_column :result_heads, :date1, :date, default: nil, after: :decimal5
    add_column :result_heads, :date2, :date, default: nil, after: :date1
    add_column :result_heads, :date3, :date, default: nil, after: :date2
    add_column :result_heads, :date4, :date, default: nil, after: :date3
    add_column :result_heads, :date5, :date, default: nil, after: :date4
  end
  def down
    # 文字予備
    remove_column :result_heads, :string1
    remove_column :result_heads, :string2
    remove_column :result_heads, :string3
    remove_column :result_heads, :string4
    remove_column :result_heads, :string5
    # 数字予備
    remove_column :result_heads, :decimal1
    remove_column :result_heads, :decimal2
    remove_column :result_heads, :decimal3
    remove_column :result_heads, :decimal4
    remove_column :result_heads, :decimal5
    # 日付
    remove_column :result_heads, :date1
    remove_column :result_heads, :date2
    remove_column :result_heads, :date3
    remove_column :result_heads, :date4
    remove_column :result_heads, :date5
  end
end
