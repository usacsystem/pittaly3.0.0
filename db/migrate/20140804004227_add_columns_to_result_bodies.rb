class AddColumnsToResultBodies < ActiveRecord::Migration
  def up
    # 文字予備
    add_column :result_bodies, :string1, :string, default: "", after: :quantity
    add_column :result_bodies, :string2, :string, default: "", after: :string1
    add_column :result_bodies, :string3, :string, default: "", after: :string2
    add_column :result_bodies, :string4, :string, default: "", after: :string3
    add_column :result_bodies, :string5, :string, default: "", after: :string4
    # 数字予備
    add_column :result_bodies, :decimal1, :decimal, default: 0, precision: 13, scale: 3, after: :string5
    add_column :result_bodies, :decimal2, :decimal, default: 0, precision: 13, scale: 3, after: :decimal1
    add_column :result_bodies, :decimal3, :decimal, default: 0, precision: 13, scale: 3, after: :decimal2
    add_column :result_bodies, :decimal4, :decimal, default: 0, precision: 13, scale: 3, after: :decimal3
    add_column :result_bodies, :decimal5, :decimal, default: 0, precision: 13, scale: 3, after: :decimal4
    # 日付
    add_column :result_bodies, :date1, :date, default: nil, after: :decimal5
    add_column :result_bodies, :date2, :date, default: nil, after: :date1
    add_column :result_bodies, :date3, :date, default: nil, after: :date2
    add_column :result_bodies, :date4, :date, default: nil, after: :date3
    add_column :result_bodies, :date5, :date, default: nil, after: :date4
  end
  def down
    # 文字予備
    remove_column :result_bodies, :string1
    remove_column :result_bodies, :string2
    remove_column :result_bodies, :string3
    remove_column :result_bodies, :string4
    remove_column :result_bodies, :string5
    # 数字予備
    remove_column :result_bodies, :decimal1
    remove_column :result_bodies, :decimal2
    remove_column :result_bodies, :decimal3
    remove_column :result_bodies, :decimal4
    remove_column :result_bodies, :decimal5
    # 日付
    remove_column :result_bodies, :date1
    remove_column :result_bodies, :date2
    remove_column :result_bodies, :date3
    remove_column :result_bodies, :date4
    remove_column :result_bodies, :date5
  end
end
