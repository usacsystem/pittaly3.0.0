class CreateLicenses < ActiveRecord::Migration
  def change
    create_table :licenses do |t|
      t.references  :user
      t.string      :name
      t.string      :account
      t.string      :password_hash
      t.string      :password_salt
      t.string      :uuid
      t.boolean     :activation,      default: true
      t.timestamps
    end

    add_index :licenses, [:user_id, :account, :uuid],
      name:     'primary_key',
      unique:   true
  end
end
