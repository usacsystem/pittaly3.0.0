class CreateOffices < ActiveRecord::Migration
  def change
    create_table :offices do |t|
      t.references  :user
      t.integer     :cd,    null: false, default: 0
      t.string      :name,  null: false, default: ""
      t.timestamps
    end

    add_index :offices, [:user_id, :cd],
      name:     'primary_index',
      unique:   true
  end
end
