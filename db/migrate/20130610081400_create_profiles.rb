class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.references    :user
      t.string        :person,    null: false
      t.string        :company,   null: false
      t.string        :zipcd
      t.string        :state
      t.string        :address
      t.string        :building
      t.string        :telno
      t.string        :faxno
      t.timestamps
    end
  end
end
