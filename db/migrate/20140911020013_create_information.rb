class CreateInformation < ActiveRecord::Migration
  def change
    create_table :information do |t|
      t.integer       :important_kbn,   null: false,  default: 0
      t.string        :title,           null: false,  default: ""
      t.text          :article,         null: false
      t.timestamps
    end
  end
end
