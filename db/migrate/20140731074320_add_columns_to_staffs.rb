class AddColumnsToStaffs < ActiveRecord::Migration
  def up
    add_column :staffs, :office_cd, :integer, default: 0, after: :name
  end
  def down
    remove_column :staffs, :office_cd
  end
end
