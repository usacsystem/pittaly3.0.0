class CreateResultBodies < ActiveRecord::Migration
  def change
    create_table :result_bodies do |t|
      t.references  :result_head
      t.time        :result_time
      t.string      :skucd
      t.integer     :row,       null: false, default: 0
      t.integer     :quantity,  null: false, default: 0
      t.timestamps
    end

    add_index :result_bodies, [:result_head_id, :row],
      name:     'primary_index',
      unique:   true
  end
end
