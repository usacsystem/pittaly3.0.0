class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.references    :license
      t.string        :name
      t.string        :architecture
      t.string        :density
      t.integer       :dpi
      t.float         :height
      t.float         :width
      t.string        :locale
      t.string        :model
      t.string        :platform
      t.string        :osname
      t.string        :ostype
      t.integer       :processor
      t.string        :version
      t.string        :appversion
      t.timestamps
    end

    add_index :devices, :license_id,
      name:     'primary_index',
      unique:   true
  end
end
