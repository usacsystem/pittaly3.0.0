class AddIndexToItems < ActiveRecord::Migration
  def change
    add_index :items, [:user_id, :jancd],
      name:     'second_index',
      unique:   true
  end
end
