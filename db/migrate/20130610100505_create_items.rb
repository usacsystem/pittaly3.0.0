class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.references  :user
      t.string      :skucd,   null: false,  default: ""
      t.string      :jancd,   null: false,  default: ""
      t.string      :name,    null: false,  default: ""
      t.string      :kind,                  default: ""
      t.integer     :price,   null: false,  default: 0
      t.integer     :carton,  null: false,  default: 1
      t.timestamps
    end

    add_index :items, [:user_id, :skucd],
      name:     'primary_index',
      unique:   true
  end
end
