class CreateResultHeads < ActiveRecord::Migration
  def change
    create_table :result_heads do |t|
      t.references  :user
      t.references  :license
      t.string      :jobno
      t.date        :result_date
      t.integer     :categorycd
      t.integer     :officecd
      t.integer     :staffcd
      t.integer     :export,  null: false, default: 0, limit: 1
      t.timestamps
    end

    add_index :result_heads, [:user_id, :jobno],
      name:     'primary_index',
      unique:   true
  end
end
