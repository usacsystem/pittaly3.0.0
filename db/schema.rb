# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150325073653) do

  create_table "admins", :force => true do |t|
    t.string   "email",              :default => "", :null => false
    t.string   "encrypted_password", :default => "", :null => false
    t.integer  "sign_in_count",      :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
  end

  add_index "admins", ["email"], :name => "index_admins_on_email", :unique => true

  create_table "categories", :force => true do |t|
    t.integer  "user_id"
    t.integer  "cd",         :default => 0,  :null => false
    t.string   "name",       :default => "", :null => false
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "categories", ["user_id", "cd"], :name => "primary_index", :unique => true

  create_table "common_bodies", :force => true do |t|
    t.integer  "common_head_id"
    t.string   "skucd",          :default => "", :null => false
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  add_index "common_bodies", ["common_head_id", "skucd"], :name => "primary_index", :unique => true

  create_table "common_heads", :force => true do |t|
    t.integer  "user_id"
    t.integer  "cd",         :default => 0,  :null => false
    t.string   "name",       :default => "", :null => false
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "common_heads", ["user_id", "cd"], :name => "primary_index", :unique => true

  create_table "devices", :force => true do |t|
    t.integer  "license_id"
    t.string   "name"
    t.string   "architecture"
    t.string   "density"
    t.integer  "dpi"
    t.float    "height"
    t.float    "width"
    t.string   "locale"
    t.string   "model"
    t.string   "platform"
    t.string   "osname"
    t.string   "ostype"
    t.integer  "processor"
    t.string   "version"
    t.string   "appversion"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "devices", ["license_id"], :name => "primary_index", :unique => true

  create_table "information", :force => true do |t|
    t.integer  "important_kbn", :default => 0,  :null => false
    t.string   "title",         :default => "", :null => false
    t.text     "article",                       :null => false
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "items", :force => true do |t|
    t.integer  "user_id"
    t.string   "skucd",      :default => "", :null => false
    t.string   "jancd",      :default => "", :null => false
    t.string   "name",       :default => "", :null => false
    t.string   "kind",       :default => ""
    t.integer  "price",      :default => 0,  :null => false
    t.integer  "carton",     :default => 1,  :null => false
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "items", ["user_id", "jancd"], :name => "second_index", :unique => true
  add_index "items", ["user_id", "skucd"], :name => "primary_index", :unique => true

  create_table "licenses", :force => true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "account"
    t.string   "password_hash"
    t.string   "password_salt"
    t.string   "uuid"
    t.boolean  "activation",    :default => true
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "licenses", ["user_id", "account", "uuid"], :name => "primary_key", :unique => true

  create_table "offices", :force => true do |t|
    t.integer  "user_id"
    t.integer  "cd",         :default => 0,  :null => false
    t.string   "name",       :default => "", :null => false
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "offices", ["user_id", "cd"], :name => "primary_index", :unique => true

  create_table "preparations", :force => true do |t|
    t.string   "type"
    t.integer  "category_id"
    t.string   "hint",        :default => ""
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "profiles", :force => true do |t|
    t.integer  "user_id"
    t.string   "person",     :null => false
    t.string   "company",    :null => false
    t.string   "zipcd"
    t.string   "state"
    t.string   "address"
    t.string   "building"
    t.string   "telno"
    t.string   "faxno"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "rails_admin_histories", :force => true do |t|
    t.text     "message"
    t.string   "username"
    t.integer  "item"
    t.string   "table"
    t.integer  "month",      :limit => 2
    t.integer  "year",       :limit => 8
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "rails_admin_histories", ["item", "table", "month", "year"], :name => "index_rails_admin_histories"

  create_table "result_bodies", :force => true do |t|
    t.integer  "result_head_id"
    t.time     "result_time"
    t.string   "skucd"
    t.integer  "row",                                           :default => 0,   :null => false
    t.integer  "quantity",                                      :default => 0,   :null => false
    t.string   "string1",                                       :default => ""
    t.string   "string2",                                       :default => ""
    t.string   "string3",                                       :default => ""
    t.string   "string4",                                       :default => ""
    t.string   "string5",                                       :default => ""
    t.decimal  "decimal1",       :precision => 13, :scale => 3, :default => 0.0
    t.decimal  "decimal2",       :precision => 13, :scale => 3, :default => 0.0
    t.decimal  "decimal3",       :precision => 13, :scale => 3, :default => 0.0
    t.decimal  "decimal4",       :precision => 13, :scale => 3, :default => 0.0
    t.decimal  "decimal5",       :precision => 13, :scale => 3, :default => 0.0
    t.date     "date1"
    t.date     "date2"
    t.date     "date3"
    t.date     "date4"
    t.date     "date5"
    t.datetime "created_at",                                                     :null => false
    t.datetime "updated_at",                                                     :null => false
  end

  add_index "result_bodies", ["result_head_id", "row"], :name => "primary_index", :unique => true

  create_table "result_heads", :force => true do |t|
    t.integer  "user_id"
    t.integer  "license_id"
    t.string   "jobno"
    t.date     "result_date"
    t.integer  "categorycd"
    t.integer  "officecd"
    t.integer  "staffcd"
    t.string   "string1",                                                 :default => ""
    t.string   "string2",                                                 :default => ""
    t.string   "string3",                                                 :default => ""
    t.string   "string4",                                                 :default => ""
    t.string   "string5",                                                 :default => ""
    t.decimal  "decimal1",                 :precision => 13, :scale => 3, :default => 0.0
    t.decimal  "decimal2",                 :precision => 13, :scale => 3, :default => 0.0
    t.decimal  "decimal3",                 :precision => 13, :scale => 3, :default => 0.0
    t.decimal  "decimal4",                 :precision => 13, :scale => 3, :default => 0.0
    t.decimal  "decimal5",                 :precision => 13, :scale => 3, :default => 0.0
    t.date     "date1"
    t.date     "date2"
    t.date     "date3"
    t.date     "date4"
    t.date     "date5"
    t.integer  "export",      :limit => 1,                                :default => 0,   :null => false
    t.datetime "created_at",                                                               :null => false
    t.datetime "updated_at",                                                               :null => false
  end

  add_index "result_heads", ["user_id", "jobno"], :name => "primary_index", :unique => true

  create_table "staffs", :force => true do |t|
    t.integer  "user_id"
    t.integer  "cd",         :default => 0,  :null => false
    t.string   "name",       :default => "", :null => false
    t.integer  "office_cd"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "staffs", ["user_id", "cd"], :name => "primary_index", :unique => true

  create_table "sync_histories", :force => true do |t|
    t.integer  "user_id"
    t.integer  "cd"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sync_histories", ["user_id", "cd"], :name => "primary_index", :unique => true

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",        :default => 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "authentication_token"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "users", ["authentication_token"], :name => "index_users_on_authentication_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["unlock_token"], :name => "index_users_on_unlock_token", :unique => true

end
