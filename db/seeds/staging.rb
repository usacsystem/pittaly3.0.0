# coding: utf-8

require 'csv'

# データベースの初期化
User.delete_all
Profile.delete_all
Item.delete_all
Staff.delete_all
Office.delete_all
Category.delete_all
License.delete_all
CommonHead.delete_all
CommonBody.delete_all

# ======================
# デモアカウント
# ======================
demo_user = User.new(email: 'demo@usknet.co.jp', password: 'manager')
demo_user.save!

# プロファイル
# モデルユーザプロファイルデータ
Profile.new(
  user_id:  demo_user.id,
  person:   'システム担当者',
  company:  'ユーザックシステム',
  zipcd:    '103-0015',
  state:    '東京都',
  address:  '中央区日本橋箱崎町４−３',
  building: '国際箱崎ビル４Ｆ',
  telno:    '0366611210',
  faxno:    '0356430909'
).save

# 商品マスタ（機種依存文字含む）
CSV.foreach("db/csv/demo_item.csv", encoding: "CP932") do |row|
  Item.user_item_import_from_csv(demo_user, row).save
end

# 担当者マスタ
CSV.foreach("db/csv/demo_staff.csv", encoding: "CP932") do |row|
  Staff.user_staff_import_from_csv(demo_user, row).save!
end

# 店舗マスタ
CSV.foreach("db/csv/demo_office.csv", encoding: "CP932") do |row|
  Office.user_office_import_from_csv(demo_user, row).save
end

# データ区分マスタ
Category.create([
  {user_id: demo_user.id, cd: 1, name: '発注'},
  {user_id: demo_user.id, cd: 2, name: '売上'},
  {user_id: demo_user.id, cd: 3, name: '棚卸'},
  {user_id: demo_user.id, cd: 4, name: '返品'},
  {user_id: demo_user.id, cd: 5, name: '廃棄'}
])

# 共通マイリストマスタ
# アップロードされたCSVを取り込む
old_cd = ""
old_name = ""
ch = nil
CSV.foreach("db/csv/demo_mylist.csv", encoding: "CP932") do |row|
  if old_cd != row[0] || old_name != row[1]
    ch = CommonHead.user_common_head_import_from_csv(demo_user, row)
    ch.save!
    old_cd = row[0]
    old_name = row[1]
  end
  cb = CommonBody.user_common_body_import_from_csv(ch, row)
  cb.save
end

# ======================
# 開発アカウント
# ======================
dev_user = User.new(email: 'dev@usknet.co.jp', password: 'manager')
dev_user.save!

# プロファイル
# モデルユーザプロファイルデータ
Profile.new(
  user_id:  dev_user.id,
  person:   'システム担当者',
  company:  '開発アカウント',
  zipcd:    '103-0015',
  state:    '東京都',
  address:  '中央区日本橋箱崎町４−３',
  building: '国際箱崎ビル４Ｆ',
  telno:    '0366611210',
  faxno:    '0356430909'
).save

# 商品マスタ（機種依存文字含む）
CSV.foreach("db/csv/demo_item.csv", encoding: "CP932") do |row|
  Item.user_item_import_from_csv(dev_user, row).save
end

# 担当者マスタ
CSV.foreach("db/csv/demo_staff.csv", encoding: "CP932") do |row|
  Staff.user_staff_import_from_csv(dev_user, row).save!
end

# 店舗マスタ
CSV.foreach("db/csv/demo_office.csv", encoding: "CP932") do |row|
  Office.user_office_import_from_csv(dev_user, row).save
end

# データ区分マスタ
Category.create([
  {user_id: dev_user.id, cd: 1, name: '発注'},
  {user_id: dev_user.id, cd: 2, name: '売上'},
  {user_id: dev_user.id, cd: 3, name: '棚卸'},
  {user_id: dev_user.id, cd: 4, name: '返品'},
  {user_id: dev_user.id, cd: 5, name: '廃棄'}
])

# 共通マイリストマスタ
# アップロードされたCSVを取り込む
old_cd = ""
old_name = ""
ch = nil
CSV.foreach("db/csv/demo_mylist.csv", encoding: "CP932") do |row|
  if old_cd != row[0] || old_name != row[1]
    ch = CommonHead.user_common_head_import_from_csv(dev_user, row)
    ch.save!
    old_cd = row[0]
    old_name = row[1]
  end
  cb = CommonBody.user_common_body_import_from_csv(ch, row)
  cb.save
end

# ===================
# モデルユーザアカウント
# ===================
# 株式会社　ちふれ化粧品
chifure_user = User.new(email: 'chifure@usknet.co.jp', password: '2eqDuaCcF^XsZ1c$Apsm')
chifure_user.save!

# モデルユーザプロファイルデータ
Profile.new(
  user_id:  chifure_user.id,
  person:   '企業システム担当者',
  company:  '株式会社　ちふれ化粧品',
  zipcd:    '350-0833',
  state:    '埼玉県',
  address:  '川越市芳野台2-8-59',
  building: '',
  telno:    '0492256101',
  faxno:    '0492256106'
).save

# モデルユーザ商品マスタ（機種依存文字含む）
CSV.foreach("db/csv/chifure_item.csv", encoding: "CP932") do |row|
  Item.user_item_import_from_csv(chifure_user, row).save
end

# モデルユーザ担当者マスタ
CSV.foreach("db/csv/demo_staff.csv", encoding: "CP932") do |row|
  Staff.user_staff_import_from_csv(chifure_user, row).save!
end

# モデルユーザ店舗マスタ
CSV.foreach("db/csv/demo_office.csv", encoding: "CP932") do |row|
  Office.user_office_import_from_csv(chifure_user, row).save
end

# モデルユーザマイリストデータ
# 価格別マイリスト
count = 0
price = Item.where('user_id = ?', chifure_user.id).group(:price)
price.each_with_index do |item, i|
  ch = CommonHead.new(user_id: chifure_user.id, cd: i + 1, name: item.price.to_s << "円商品")
  ch.save!

  Item.where('user_id = ? and price = ?', chifure_user.id, item.price).each do |item2|
    CommonBody.new(common_head_id: ch.id, skucd: item2.skucd).save
  end

  count = i + 1
end

# 分類別マイリスト
kind = Item.where('user_id = ?', chifure_user.id).group(:kind)
kind.each_with_index do |item, j|
  ch = CommonHead.new(user_id: chifure_user.id, cd: count + j + 1, name: item.kind.to_s)
  ch.save!

  Item.where('user_id = ? and kind = ?', chifure_user.id, item.kind).each do |item2|
    CommonBody.new(common_head_id: ch.id, skucd: item2.skucd).save
  end
end

# モデルユーザデータ区分データ
# 株式会社　ちふれ化粧品
Category.create([
  {user_id: chifure_user.id, cd: 1, name: '売上'},
  {user_id: chifure_user.id, cd: 2, name: '発注'},
  {user_id: chifure_user.id, cd: 3, name: '棚卸'}
])