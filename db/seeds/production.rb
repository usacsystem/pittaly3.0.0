# coding: utf-8

require 'csv'

# データベースの初期化
User.delete_all
Profile.delete_all
Item.delete_all
Staff.delete_all
Office.delete_all
Category.delete_all
License.delete_all
CommonHead.delete_all
CommonBody.delete_all

# ======================
# デモアカウント
# ======================
demo_user = User.new(email: 'ipn-dev@usknet.co.jp', password: 'msdqKaun>2VBSSzfdWDjbuAaVoScs4PDpn&knrnF')
demo_user.save!

# プロファイル
# モデルユーザプロファイルデータ
Profile.new(
  user_id:  demo_user.id,
  person:   'システム担当者',
  company:  'テストユーザ',
  zipcd:    '103-0015',
  state:    '東京都',
  address:  '中央区日本橋箱崎町４−３',
  building: '国際箱崎ビル４Ｆ',
  telno:    '0366611210',
  faxno:    '0356430909'
).save

# 商品マスタ（機種依存文字含む）
CSV.foreach("db/csv/demo_item.csv", encoding: "CP932") do |row|
  Item.user_item_import_from_csv(demo_user, row).save
end

# 担当者マスタ
CSV.foreach("db/csv/demo_staff.csv", encoding: "CP932") do |row|
  Staff.user_staff_import_from_csv(demo_user, row).save!
end

# 店舗マスタ
CSV.foreach("db/csv/demo_office.csv", encoding: "CP932") do |row|
  Office.user_office_import_from_csv(demo_user, row).save
end

# データ区分マスタ
Category.create([
  {user_id: demo_user.id, cd: 1, name: '発注'},
  {user_id: demo_user.id, cd: 2, name: '売上'},
  {user_id: demo_user.id, cd: 3, name: '棚卸'},
  {user_id: demo_user.id, cd: 4, name: '返品'},
  {user_id: demo_user.id, cd: 5, name: '廃棄'}
])

# 共通マイリストマスタ
# アップロードされたCSVを取り込む
old_cd = ""
old_name = ""
ch = nil
CSV.foreach("db/csv/demo_mylist.csv", encoding: "CP932") do |row|
  if old_cd != row[0] || old_name != row[1]
    ch = CommonHead.user_common_head_import_from_csv(demo_user, row)
    ch.save!
    old_cd = row[0]
    old_name = row[1]
  end
  cb = CommonBody.user_common_body_import_from_csv(ch, row)
  cb.save
end