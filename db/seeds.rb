# coding : utf-8
# 環境ごとにseedを分ける
# common.rbはいずれの環境でも実行される
['common', Rails.env].each do |seed|
  seed_file = "#{Rails.root}/db/seeds/#{seed}.rb"
  if File.exists?(seed_file)
    puts "*** Loading #{seed} seed data"
    require seed_file
  end
end
