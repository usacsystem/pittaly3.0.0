module ControllerMacros
  def login_user
    before(:each) do
      controller.stub(:authenticate_user!).and_return true
      @request.env["devise.mapping"] = Devise.mappings[:user]
      @user = User.create(email: "test@usknet.co.jp",
                          password: "manager",
                          password_confirmation: "manager")
      # @user.confirm!
      sign_in @user
    end
  end
end