# coding: utf-8
require 'spec_helper'

describe CommonHeadsController do
  describe 'routing' do

    describe 'get common_heads_path / common_heads#indexへのルーティングが正常' do
      subject { {get: common_heads_path} }
      it { should be_routable }
      it { should route_to controller: 'common_heads', action: 'index' }
    end

    describe 'post common_heads_path / common_heads#createへのルーティングが正常' do
      subject { {post: common_heads_path} }
      it { should be_routable }
      it { should route_to controller: 'common_heads', action: 'create' }
    end

    describe 'get new_common_head_path / common_heads#newへのルーティングが正常' do
      subject { {get: new_common_head_path} }
      it { should be_routable }
      it { should route_to controller: 'common_heads', action: 'new' }
    end

    describe 'get edit_common_head_path / common_heads#editへのルーティングが正常' do
      subject { {get: edit_common_head_path(1)} }
      it { should be_routable }
      it { should route_to controller: 'common_heads', action: 'edit', id: '1' }
    end

    describe 'put common_head_path / common_heads#updateへのルーティングが正常' do
      subject { {put: common_head_path(1)} }
      it { should be_routable }
      it { should route_to controller: 'common_heads', action: 'update', id: '1' }
    end

    describe 'delete common_head_path / common_heads#destroyへのルーティングが正常' do
      subject { {delete: common_head_path(1)} }
      it { should be_routable }
      it { should route_to controller: 'common_heads', action: 'destroy', id: '1' }
    end

    describe 'get common_head_path / common_heads#showへのルーティングは許可しない' do
      subject { {get: common_head_path(1)} }
      it { should route_to controller: 'errors', action: 'routing', a: 'mylists/1' }
    end

  end
end
