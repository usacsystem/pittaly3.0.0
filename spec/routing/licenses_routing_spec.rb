# coding: utf-8
require 'spec_helper'

describe LicensesController do
  describe 'routing' do

    describe 'get licenses_path / licenses#indexへのルーティングが正常' do
      subject { {get: licenses_path} }
      it { should be_routable }
      it { should route_to controller: 'licenses', action: 'index' }
    end

    describe 'post licenses_path / licenses#createへのルーティングが正常' do
      subject { {post: licenses_path} }
      it { should be_routable }
      it { should route_to controller: 'licenses', action: 'create' }
    end

    describe 'get new_license_path / licenses#newへのルーティングが正常' do
      subject { {get: new_license_path} }
      it { should be_routable }
      it { should route_to controller: 'licenses', action: 'new' }
    end

    describe 'get edit_license_path / licenses#editへのルーティングが正常' do
      subject { {get: edit_license_path(1)} }
      it { should be_routable }
      it { should route_to controller: 'licenses', action: 'edit', id: '1' }
    end

    describe 'put license_path / licenses#updateへのルーティングが正常' do
      subject { {put: license_path(1)} }
      it { should be_routable }
      it { should route_to controller: 'licenses', action: 'update', id: '1' }
    end

    describe 'delete license_path / licenses#destroyへのルーティングは許可しない' do
      subject { {delete: license_path(1)} }
      it { should route_to controller: 'errors', action: 'routing', a: 'licenses/1' }
    end

    describe 'get license_path / licenses#showへのルーティングは許可しない' do
      subject { {get: license_path(1)} }
      it { should route_to controller: 'errors', action: 'routing', a: 'licenses/1' }
    end

    describe 'licenses resourcesのメンバーのテスト' do

      describe 'post stop_license_path / licenses#stopへのルーティングが正常' do
        subject { {post: stop_license_path(1)} }
        it { should be_routable }
        it { should route_to controller: 'licenses', action: 'stop', id: '1' }
      end

      describe 'post start_license_path / licenses#startのルーティングが正常' do
        subject { {post: start_license_path(1)} }
        it { should be_routable }
        it { should route_to controller: 'licenses', action: 'start', id: '1' }
      end

    end

  end
end
