# coding: utf-8
require 'spec_helper'

describe ItemsController do
  describe 'routing' do

    describe 'get items_path / items#indexへのルーティングが正常' do
      subject { {get: items_path} }
      it { should be_routable }
      it { should route_to controller: 'items', action: 'index' }
    end

    describe 'post items_path / items#createへのルーティングが正常' do
      subject { {post: items_path} }
      it { should be_routable }
      it { should route_to controller: 'items', action: 'create' }
    end

    describe 'get new_item_path / items#newへのルーティングが正常' do
      subject { {get: new_item_path} }
      it { should be_routable }
      it { should route_to controller: 'items', action: 'new' }
    end

    describe 'get edit_item_path / items#editへのルーティングが正常' do
      subject { {get: edit_item_path(1)} }
      it { should be_routable }
      it { should route_to controller: 'items', action: 'edit', id: '1' }
    end

    describe 'put item_path / items#updateへのルーティングが正常' do
      subject { {put: item_path(1)} }
      it { should be_routable }
      it { should route_to controller: 'items', action: 'update', id: '1' }
    end

    describe 'delete item_path / items#destroyへのルーティングが正常' do
      subject { {delete: item_path(1)} }
      it { should be_routable }
      it { should route_to controller: 'items', action: 'destroy', id: '1' }
    end

    describe 'get item_path / items#showへのルーティングは許可しない' do
      subject { {get: item_path(1)} }
      it { should route_to controller: 'errors', action: 'routing', a: 'items/1' }
    end

    describe 'items resourcesのコレクションのテスト' do

      describe 'get search_items_path / items#searchへのルーティングが正常' do
        subject { {get: search_items_path} }
        it { should be_routable }
        it { should route_to controller: 'items', action: 'search' }
      end

    end

  end
end
