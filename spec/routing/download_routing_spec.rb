# coding: utf-8
require 'spec_helper'

describe DownloadController do
  describe 'routing' do

    describe 'get download_path / download#searchへのルーティングが正常' do
      subject { {get: download_path} }
      it { should be_routable }
      it { should route_to controller: 'download', action: 'search' }
    end

    describe 'get download_search_path / download#searchへのルーティングが正常' do
      subject { {get: download_search_path} }
      it { should be_routable }
      it { should route_to controller: 'download', action: 'search' }
    end

    describe 'get download_export_file_path / download#export_fileへのルーティングが正常' do
      subject { {get: download_export_file_path} }
      it { should be_routable }
      it { should route_to controller: 'download', action: 'export_file'}
    end

  end
end
