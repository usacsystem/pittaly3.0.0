# coding: utf-8
require 'spec_helper'

describe UploadController do
  describe 'routing' do

    describe 'get upload_path / upload#import_fileへのルーティングが正常' do
      subject { {get: upload_path} }
      it { should be_routable }
      it { should route_to controller: 'upload', action: 'import_file' }
    end

  end
end
