# coding: utf-8
require 'spec_helper'

describe StaffsController do
  describe 'routing' do

    describe 'get staffs_path / staffs#indexへのルーティングが正常' do
      subject { {get: staffs_path} }
      it { should be_routable }
      it { should route_to controller: 'staffs', action: 'index' }
    end

    describe 'post staffs_path / staffs#createへのルーティングが正常' do
      subject { {post: staffs_path} }
      it { should be_routable }
      it { should route_to controller: 'staffs', action: 'create' }
    end

    describe 'get new_staff_path / staffs#newへのルーティングが正常' do
      subject { {get: new_staff_path} }
      it { should be_routable }
      it { should route_to controller: 'staffs', action: 'new' }
    end

    describe 'get edit_staff_path / staffs#editへのルーティングが正常' do
      subject { {get: edit_staff_path(1)} }
      it { should be_routable }
      it { should route_to controller: 'staffs', action: 'edit', id: '1' }
    end

    describe 'put staff_path / staffs#updateへのルーティングが正常' do
      subject { {put: staff_path(1)} }
      it { should be_routable }
      it { should route_to controller: 'staffs', action: 'update', id: '1' }
    end

    describe 'delete staff_path / staffs#destroyへのルーティングが正常' do
      subject { {delete: staff_path(1)} }
      it { should be_routable }
      it { should route_to controller: 'staffs', action: 'destroy', id: '1' }
    end

    describe 'get staff_path / staffs#showへのルーティングは許可しない' do
      subject { {get: staff_path(1)} }
      it { should route_to controller: 'errors', action: 'routing', a: 'staffs/1' }
    end

  end
end
