# coding: utf-8
require 'spec_helper'

describe OfficesController do
  describe 'routing' do

    describe 'get offices_path / offices#indexへのルーティングが正常' do
      subject { {get: offices_path} }
      it { should be_routable }
      it { should route_to controller: 'offices', action: 'index' }
    end

    describe 'post offices_path / offices#createへのルーティングが正常' do
      subject { {post: offices_path} }
      it { should be_routable }
      it { should route_to controller: 'offices', action: 'create' }
    end

    describe 'get new_office_path / offices#newへのルーティングが正常' do
      subject { {get: new_office_path} }
      it { should be_routable }
      it { should route_to controller: 'offices', action: 'new' }
    end

    describe 'get edit_office_path / offices#editへのルーティングが正常' do
      subject { {get: edit_office_path(1)} }
      it { should be_routable }
      it { should route_to controller: 'offices', action: 'edit', id: '1' }
    end

    describe 'put office_path / offices#updateへのルーティングが正常' do
      subject { {put: office_path(1)} }
      it { should be_routable }
      it { should route_to controller: 'offices', action: 'update', id: '1' }
    end

    describe 'delete office_path / offices#destroyへのルーティングが正常' do
      subject { {delete: office_path(1)} }
      it { should be_routable }
      it { should route_to controller: 'offices', action: 'destroy', id: '1' }
    end

    describe 'get office_path / offices#showへのルーティングは許可しない' do
      subject { {get: office_path(1)} }
      it { should route_to controller: 'errors', action: 'routing', a: 'offices/1' }
    end

  end
end
