# coding: utf-8
require 'spec_helper'

describe ResultsController do
  describe 'routing' do

    describe 'get results_path / results#indexへのルーティングが正常' do
      subject { {get: results_path} }
      it { should be_routable }
      it { should route_to controller: 'results', action: 'index' }
    end

  end
end
