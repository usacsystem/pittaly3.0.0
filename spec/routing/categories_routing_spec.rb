# coding: utf-8
require 'spec_helper'

describe CategoriesController do
  describe 'routing' do

    describe 'get categories_path / categories#indexへのルーティングが正常' do
      subject { {get: categories_path} }
      it { should be_routable }
      it { should route_to controller: 'categories', action: 'index' }
    end

    describe 'post categories_path / categories#createへのルーティングが正常' do
      subject { {post: categories_path} }
      it { should be_routable }
      it { should route_to controller: 'categories', action: 'create' }
    end

    describe 'get new_category_path / categories#newへのルーティングが正常' do
      subject { {get: new_category_path} }
      it { should be_routable }
      it { should route_to controller: 'categories', action: 'new' }
    end

    describe 'get edit_category_path / categories#editへのルーティングが正常' do
      subject { {get: edit_category_path(1)} }
      it { should be_routable }
      it { should route_to controller: 'categories', action: 'edit', id: '1' }
    end

    describe 'put category_path / categories#updateへのルーティングが正常' do
      subject { {put: category_path(1)} }
      it { should be_routable }
      it { should route_to controller: 'categories', action: 'update', id: '1' }
    end

    describe 'delete category_path / categories#destroyへのルーティングが正常' do
      subject { {delete: category_path(1)} }
      it { should be_routable }
      it { should route_to controller: 'categories', action: 'destroy', id: '1' }
    end

    describe 'get category_path / categories#showへのルーティングは許可しない' do
      subject { {get: category_path(1)} }
      it { should route_to controller: 'errors', action: 'routing', a: 'categories/1' }
    end

  end
end
