require 'spec_helper'

describe "common_heads/index" do
  before(:each) do
    assign(:common_heads, [
      stub_model(CommonHead,
        :user => nil,
        :cd => 1,
        :name => "Name"
      ),
      stub_model(CommonHead,
        :user => nil,
        :cd => 1,
        :name => "Name"
      )
    ])
  end

  it "renders a list of common_heads" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
