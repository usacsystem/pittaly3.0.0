require 'spec_helper'

describe "common_heads/new" do
  before(:each) do
    assign(:common_head, stub_model(CommonHead,
      :user => nil,
      :cd => 1,
      :name => "MyString"
    ).as_new_record)
  end

  it "renders new common_head form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", common_heads_path, "post" do
      assert_select "input#common_head_user[name=?]", "common_head[user]"
      assert_select "input#common_head_cd[name=?]", "common_head[cd]"
      assert_select "input#common_head_name[name=?]", "common_head[name]"
    end
  end
end
