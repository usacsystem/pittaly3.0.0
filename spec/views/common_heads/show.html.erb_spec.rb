require 'spec_helper'

describe "common_heads/show" do
  before(:each) do
    @common_head = assign(:common_head, stub_model(CommonHead,
      :user => nil,
      :cd => 1,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/1/)
    rendered.should match(/Name/)
  end
end
