require 'spec_helper'

describe "items/show" do
  before(:each) do
    @item = assign(:item, stub_model(Item,
      :user => nil,
      :skucd => "Skucd",
      :jancd => "Jancd",
      :name => "Name",
      :kind => "Kind",
      :price => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/Skucd/)
    rendered.should match(/Jancd/)
    rendered.should match(/Name/)
    rendered.should match(/Kind/)
    rendered.should match(/1/)
  end
end
