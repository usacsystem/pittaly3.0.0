require 'spec_helper'

describe "items/new" do
  before(:each) do
    assign(:item, stub_model(Item,
      :user => nil,
      :skucd => "MyString",
      :jancd => "MyString",
      :name => "MyString",
      :kind => "MyString",
      :price => 1
    ).as_new_record)
  end

  it "renders new item form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", items_path, "post" do
      assert_select "input#item_user[name=?]", "item[user]"
      assert_select "input#item_skucd[name=?]", "item[skucd]"
      assert_select "input#item_jancd[name=?]", "item[jancd]"
      assert_select "input#item_name[name=?]", "item[name]"
      assert_select "input#item_kind[name=?]", "item[kind]"
      assert_select "input#item_price[name=?]", "item[price]"
    end
  end
end
