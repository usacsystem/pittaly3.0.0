require 'spec_helper'

describe "staffs/edit" do
  before(:each) do
    @staff = assign(:staff, stub_model(Staff,
      :user => nil,
      :cd => 1,
      :name => "MyString"
    ))
  end

  it "renders the edit staff form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", staff_path(@staff), "post" do
      assert_select "input#staff_user[name=?]", "staff[user]"
      assert_select "input#staff_cd[name=?]", "staff[cd]"
      assert_select "input#staff_name[name=?]", "staff[name]"
    end
  end
end
