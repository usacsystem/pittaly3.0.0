require 'spec_helper'

describe "offices/edit" do
  before(:each) do
    @office = assign(:office, stub_model(Office,
      :user => nil,
      :cd => 1,
      :name => "MyString"
    ))
  end

  it "renders the edit office form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", office_path(@office), "post" do
      assert_select "input#office_user[name=?]", "office[user]"
      assert_select "input#office_cd[name=?]", "office[cd]"
      assert_select "input#office_name[name=?]", "office[name]"
    end
  end
end
