require 'spec_helper'

describe "licenses/show" do
  before(:each) do
    @license = assign(:license, stub_model(License,
      :user => nil,
      :name => "Name",
      :account => "Account",
      :password_hash => "Password Hash",
      :password_salt => "Password Salt",
      :uuid => "Uuid"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(/Name/)
    rendered.should match(/Account/)
    rendered.should match(/Password Hash/)
    rendered.should match(/Password Salt/)
    rendered.should match(/Uuid/)
  end
end
