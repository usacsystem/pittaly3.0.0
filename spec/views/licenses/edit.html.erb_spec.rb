require 'spec_helper'

describe "licenses/edit" do
  before(:each) do
    @license = assign(:license, stub_model(License,
      :user => nil,
      :name => "MyString",
      :account => "MyString",
      :password_hash => "MyString",
      :password_salt => "MyString",
      :uuid => "MyString"
    ))
  end

  it "renders the edit license form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", license_path(@license), "post" do
      assert_select "input#license_user[name=?]", "license[user]"
      assert_select "input#license_name[name=?]", "license[name]"
      assert_select "input#license_account[name=?]", "license[account]"
      assert_select "input#license_password_hash[name=?]", "license[password_hash]"
      assert_select "input#license_password_salt[name=?]", "license[password_salt]"
      assert_select "input#license_uuid[name=?]", "license[uuid]"
    end
  end
end
