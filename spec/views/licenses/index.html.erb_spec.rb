require 'spec_helper'

describe "licenses/index" do
  before(:each) do
    assign(:licenses, [
      stub_model(License,
        :user => nil,
        :name => "Name",
        :account => "Account",
        :password_hash => "Password Hash",
        :password_salt => "Password Salt",
        :uuid => "Uuid"
      ),
      stub_model(License,
        :user => nil,
        :name => "Name",
        :account => "Account",
        :password_hash => "Password Hash",
        :password_salt => "Password Salt",
        :uuid => "Uuid"
      )
    ])
  end

  it "renders a list of licenses" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Account".to_s, :count => 2
    assert_select "tr>td", :text => "Password Hash".to_s, :count => 2
    assert_select "tr>td", :text => "Password Salt".to_s, :count => 2
    assert_select "tr>td", :text => "Uuid".to_s, :count => 2
  end
end
