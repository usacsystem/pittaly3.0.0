require "spec_helper"

describe Api::V1::SendMail do
  describe "device_csv" do
    let(:mail) { Api::V1::SendMail.device_csv }

    it "renders the headers" do
      mail.subject.should eq("Device csv")
      mail.to.should eq(["to@example.org"])
      mail.from.should eq(["from@example.com"])
    end

    it "renders the body" do
      mail.body.encoded.should match("Hi")
    end
  end

end
