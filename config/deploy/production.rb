# coding: utf-8
# =======
# Servers
# =======
server '180.37.183.215', :app, :web, :db, primary: true

# ===
# SSH
# ===
set :user,                        'pittaly'
set :use_sudo,                    false
ssh_options[:port] = 10022
ssh_options[:keys] = '/Users/horus/.ssh/pittalyso_web_rsa'
default_run_options[:pty] = true

set :normalize_asset_timestamps,  false
set :keep_releases, 5

set :branch,                      'production-2.0.2'
set :rails_env,                   'production'
set :deploy_to,                   '/var/www/pittalyso_cloud'
set :deploy_via,                  :copy

# =======================
# Apache-Passengerの再起動
# =======================
after 'deploy', 'deploy:link_files'
namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
  task :link_files do
    run <<-CMD
      cd #{shared_path} &&
      mkdir -p files &&
      cd #{release_path} &&
      rm -rf #{release_path}/files &&
      ln -nfs #{shared_path}/files #{release_path}/files
    CMD
  end
end