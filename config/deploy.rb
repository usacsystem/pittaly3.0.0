# coding: utf-8

# ==========
# Capistrano
# ==========
require 'capistrano/ext/multistage'

# ===========
# Application
# ===========
set :application, 'Pittaly店舗発注'

# ==============
# Git Repository
# ==============
set :scm, :git
set :repository,  'git@bitbucket.org:usacsystem/pittalyso_cloud.git'

# =======
# Servers
# =======
# 各レシピに記載

# =====
# rbenv
# =====
require 'capistrano-rbenv'
set :rbenv_ruby_version, '1.9.3-p194'
namespace :rbenv do
  task :setup_shellenv do
    set :default_environment, {
      'RBENV_ROOT' => "#{rbenv_path}",
      'PATH' => "#{rbenv_path}/shims:#{rbenv_path}/bin:$PATH",
    }
  end
  after 'multistage:ensure', 'rbenv:setup_shellenv'
end

# ===================
# bundle installの実行
# ===================
require 'bundler/capistrano'
set :bundle_flags,  ''

# =================
# deployタスクの追加
# =================
namespace :deploy do
  desc "Load the seed data from db/seeds.rb"
  task :seed do
    run "cd #{current_path}; bundle exec rake db:seed RAILS_ENV=#{rails_env}"
  end
end

namespace :db do
  desc "run db:migrate:reset"
  task :migrate_reset do
    run "cd #{current_path}; bundle exec rake db:migrate:reset RAILS_ENV=#{rails_env}"
  end
end