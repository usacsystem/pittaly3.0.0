# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
PittalysoCloud::Application.initialize!

# Devise Mail
PittalysoCloud::Application.configure do
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.default charset: "utf-8"
  config.action_mailer.perform_deliveries = true
  config.action_mailer.smtp_settings = {
    address:          'mail.pittaly.com',
    port:             587,
    authentication:   :plain,
    user_name:        'info',
    password:         'infopit'
  }
end