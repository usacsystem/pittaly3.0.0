# coding: utf-8
PittalysoCloud::Application.routes.draw do
  devise_for :admins
  mount RailsAdmin::Engine => '/admin', :as => 'rails_admin'
  devise_for :users

  # （Web　=>　Resources）
  resources :items,             only: [:index, :new, :create, :edit, :update, :destroy] do
    collection do
      get 'search'
    end
  end
  resources :categories,        only: [:index, :new, :create, :edit, :update, :destroy]
  resources :staffs,            only: [:index, :new, :create, :edit, :update, :destroy]
  resources :offices,           only: [:index, :new, :create, :edit, :update, :destroy]
  resources :licenses,          only: [:index, :new, :create, :edit, :update] do
    member do
      post 'stop'
      post 'start'
    end
  end
  resources :common_heads,      only: [:index, :new, :create, :edit, :update, :destroy], path: 'mylists'
  resources :results,           only: [:index], path: 'dashboard'

  match :upload, to: 'upload#import_file'
  match :download, to: 'download#search'
  get   'download/search'
  get   'download/export_file'
  get   'downloal/model'

  # （端末　=>　Resources）
  namespace :api do
    namespace :v1 do
      resources :tokens,            only: [:create]
      resources :items,             only: [:index]
      resources :staffs,            only: [:index]
      resources :offices,           only: [:index]
      resources :categories,        only: [:index]
      resources :common_heads,      only: [:index], path: 'mylists'
      resources :results,           only: [:create]
      resources :sync_histories,    only: [:index]
      get 'licenses/check'
      post "send_mail/device_csv"
    end
  end

  root to: 'results#index'

  # Static Pages
  match ':action', to: 'static#:action'

  # Routing Error処理
  match '*a', to: 'errors#routing'
end
