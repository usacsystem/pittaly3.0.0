# coding: utf-8
class ItemsController < ApplicationController
  before_filter :authenticate_user!
  respond_to :html

  # GET /items
  def index
    gon.url = items_url
    @items = Item.user_items(current_user)
            .page(params[:page])
            .per(params[:per])
            .order(:skucd)
    if params[:keyword]
      wc = "%"
      keyword = wc + params[:keyword] + wc
      @items = @items.where("name like ? or kind like ?", keyword, keyword)
    end
    respond_with @items
  end

  # GET /items/new
  def new
    @item = Item.user_new(current_user)
    respond_with @item
  end

  # GET /items/1/edit
  def edit
    @item = Item.user_item(current_user, params[:id])
    # ユーザが保持するID以外を指定した場合は、一覧ページにリダイレクトさせる
    redirect_to items_url, alert: t('controllers.items.alert.find') and return if @item.blank?

    respond_with @item
  end

  # POST /items
  def create
    @item = Item.user_item_create(current_user, params[:item])

    begin
      ActiveRecord::Base.transaction() do
        if @item.save
          create_or_update_sync_history(SYNC_HISTORY_ITEM)
          redirect_to items_url, notice: t('controllers.items.notice.create')
        else
          render action: "new"
        end
      end
    rescue => e
      puts e.message
      raise_save_error(@item)
    end
  end

  # PUT /items/1
  def update
    @item = Item.user_item(current_user, params[:id])

    begin
      ActiveRecord::Base.transaction() do
        if @item.update_attributes(params[:item])
          create_or_update_sync_history(SYNC_HISTORY_ITEM)
          redirect_to items_url, notice: t('controllers.items.notice.update')
        else
          render action: "edit"
        end
      end
    rescue => e
      puts e.message
      raise_save_error(@item)
    end
  end

  # DELETE /items/1
  def destroy
    @item = Item.user_item(current_user, params[:id])

    begin
      ActiveRecord::Base.transaction() do
        @item.destroy
        create_or_update_sync_history(SYNC_HISTORY_ITEM)
        redirect_to items_url, notice: t('controllers.items.notice.destroy')
      end
    rescue => e
      puts e.message
      redirect_to items_url, notice: t('exception.delete_error')
    end

  end

  # GET /items/search
  # params: skucd
  def search
    is_jan = params[:is_jan]

    if is_jan === "true"
      @item = Item.user_item_by_jancd(current_user, params[:skucd])
    else
      @item = Item.user_item_by_skucd(current_user, params[:skucd])
    end

    if @item.nil?
      render json: { message: 'not found' } and return
    end
    # jsonを返す為、個別指定
    respond_with (@item) do |format|
      format.json
    end
  end

end