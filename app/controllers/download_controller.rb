# coding: utf-8
class DownloadController < ApplicationController
  before_filter :authenticate_user!
  respond_to :html

  def search
    if params[:utf8].nil?
      return
    end

    puts params

    @results = ResultHead.search_results(current_user, params)
    session[:download] = params
    respond_with @results, partial: 'search_result'
  end

  def export_file
    if session[:download].nil?
      return
    end

    results_csv = ResultHead.csv(current_user, session[:download])
    session[:download] = nil
    send_data results_csv,
              type: 'text/csv; charset=shift_jis',
              filename: "data.csv"
  end
end
