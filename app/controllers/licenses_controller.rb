# coding: utf-8
class LicensesController < ApplicationController
  before_filter :authenticate_user!
  respond_to :html

  # GET /licenses
  def index
    @licenses = License.user_licenses(current_user).order_by_id
    respond_with @licenses
  end

  # GET /licenses/new
  def new
    @license = License.user_new(current_user)
    respond_with @license
  end

  # GET /licenses/1/edit
  def edit
    @license = License.user_license(current_user, params[:id])
    # ユーザが保持するID以外を指定した場合は、一覧ページにリダイレクトさせる
    redirect_to licenses_url, alert: t('controllers.licenses.alert.find') and return if @license.blank?

    respond_with @license
  end

  # POST /licenses
  def create
    @license = License.user_license_create(current_user, params[:license])

    if @license.save
      redirect_to licenses_path, notice: t('controllers.licenses.notice.create')
    else
      render action: "new"
    end
  end

  # PUT /licenses/1
  def update
    @license = License.user_license(current_user, params[:id])

    # UUIDを変更した場合は、Device情報を削除
    if (@license.uuid != params[:license][:uuid]) && !@license.device.nil?
      @license.device.destroy()
    end

    if @license.update_attributes(params[:license])
      redirect_to licenses_path, notice: t('controllers.licenses.notice.update')
    else
      render action: "edit"
    end
  end

  # POST /licenses/1/stop
  def stop
    ret = License.activate_license(current_user, params[:id], false)
    render json: ret
  end

  # POST /licenses/1/start
  def start
    ret = License.activate_license(current_user, params[:id], true)
    render json: ret
  end

end
