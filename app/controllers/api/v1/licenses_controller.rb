# coding: utf-8
class Api::V1::LicensesController < ApplicationController
  before_filter :authenticate_user!
  respond_to :json

  # GET api/v1/licenses/check
  def check

    # json以外のリクエストは406を返す
    if request.format != :json
      render status: 406, json: { message: 'The request must be json format.' } and return
    end

    # 各パラメータがnilならば400を返す
    if params[:account_uuid].nil?
      render status: 400, json: { message: 'The request must contain the account_uuid.' } and return
    end

    ret = License.user_license_count_by_uuid(current_user, params[:account_uuid])
    if ret == 0
      render status: 200, json: { message: "NG" } and return
    else
      render status: 200, json: { message: "OK" } and return
    end

  end

end
