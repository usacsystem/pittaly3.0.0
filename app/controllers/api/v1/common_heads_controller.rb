# coding: utf-8
class Api::V1::CommonHeadsController < ApplicationController
  before_filter :authenticate_user!
  respond_to :json

  def index
    @common_heads = CommonHead.user_common_heads(current_user).order_by_id
    respond_with @common_heads
  end
end
