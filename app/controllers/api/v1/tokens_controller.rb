# coding: utf-8
class Api::V1::TokensController < ApplicationController
  skip_before_filter :verify_authenticity_token
  respond_to :json

  require 'bcrypt'

  def create
    data = ActiveSupport::JSON.decode(request.body)

    # 認証情報
    account_id = data['account_id']
    account_pw = data['account_pw']
    account_uuid = data['account_uuid']
    # 端末情報
    device_info = data['device_info']

    # json以外のリクエストは406を返す
    if request.format != :json
      render status: 406, json: { message: 'The request must be json format.' } and return
    end

    # 各パラメータがnilならば400を返す
    if account_id.nil? or account_pw.nil? or account_uuid.nil?
      render status: 400, json: { message: 'The request must contain the account_id and account_pw and account_uuid.' } and return
    end

    # リクエストパラメータでUserを検索する
    user = searchAuthenticateUser(account_id, account_pw, account_uuid)

    # ユーザがnilならば401エラーを返す
    if user.nil?
      render status: 401, json: { message: 'Invalid account_id or account_pw or account_uuid' } and return
    end

    # Licenseに紐付くDeviceを登録
    license = searchLicense(account_id, account_pw, account_uuid)

    # もしLicenseに紐付くDevice情報が既にある場合は、
    # Device情報の更新、無ければ新規登録する
    begin
      if license.device.nil?
        device = Device.license_device_create(license, device_info)
        device.save
      else
        device = license.device
        device.update_attributes(device_info)
      end
    rescue => e
      # 保存に失敗したらエラー
      render status: 401, json: { message: 'Invalid device_info' } and return
    end

    # gem deviseからuserのAccessTokenを発行する
    # 端末には200を返し、AccessTokenを通知する
    user.ensure_authentication_token
    user.save!(validate: false)
    render status: 200, json: { token: user.authentication_token }

  end

  private

  # account_id / account_pw / account_uuidでLicenseを検索
  # Licenseを返す
  def searchLicense(_account_id, _account_pw, _account_uuid)
    license =  License.where(['account = ? and uuid = ?', _account_id, _account_uuid]).activate_licenses(true).first
    if license && license.password_hash == BCrypt::Engine.hash_secret(_account_pw, license.password_salt)
      return license
    else
      nil
    end
  end

  # 関連先のUserを返す
  def searchAuthenticateUser(_account_id, _account_pw, _account_uuid)
    license = searchLicense(_account_id, _account_pw, _account_uuid)
    if license.nil?
      nil
    else
      user = license.user
    end
  end

end