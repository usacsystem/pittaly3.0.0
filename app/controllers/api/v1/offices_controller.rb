# coding: utf-8
class Api::V1::OfficesController < ApplicationController
  before_filter :authenticate_user!
  respond_to :json

  # ユーザの店舗一覧を出力する
  def index
    @offices = Office.user_offices(current_user).order_by_id
    respond_with @offices
  end
end
