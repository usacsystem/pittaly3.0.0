# coding: utf-8
class Api::V1::CategoriesController < ApplicationController
  before_filter :authenticate_user!
  respond_to :json

  # ユーザのデータ区分一覧を出力する
  def index
    @categories = Category.user_categories(current_user).order_by_id
    respond_with @categories
  end
end
