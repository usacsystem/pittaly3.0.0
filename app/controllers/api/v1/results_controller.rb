# coding: utf-8
class Api::V1::ResultsController < ApplicationController
  before_filter :authenticate_user!

  # 端末から実績データを受け取り保存する。
  def create
    data = ActiveSupport::JSON.decode(request.body)

    # Validation

    # Logic
    count = 0
    jobno = ""
    begin
      ActiveRecord::Base.transaction() do
        # 端末UUIDからLicenseID取得
        license = License.user_license_by_uuid(current_user, data['account_uuid'])

        # 次のジョブNoを取得
        jobno = getNextJobNo

        # 実績ヘッダデータの登録
        result_head = ResultHead.new({
          user_id:      current_user.id,
          license_id:   license.id,
          jobno:        jobno,
          result_date:  data['result_date'],
          categorycd:   data['categorycd'],
          staffcd:      data['staffcd'],
          officecd:     data['officecd'],
          string1:      data["string1"],
          string2:      data["string2"],
          string3:      data["string3"],
          string4:      data["string4"],
          string5:      data["string5"],
          decimal1:     data["decimal1"],
          decimal2:     data["decimal2"],
          decimal3:     data["decimal3"],
          decimal4:     data["decimal4"],
          decimal5:     data["decimal5"],
          date1:        data["date1"],
          date2:        data["date2"],
          date3:        data["date3"],
          date4:        data["date4"],
          date5:        data["date5"],
          export:       0
        })
        result_head.save!

        # 実績明細データの登録
        data['body'].each do |body|
          result_body = ResultBody.new({
            result_head_id:   result_head.id,
            result_time:      body['result_time'],
            row:              body['row'],
            skucd:            body['skucd'],
            quantity:         body['quantity'],
            string1:          body["string1"],
            string2:          body["string2"],
            string3:          body["string3"],
            string4:          body["string4"],
            string5:          body["string5"],
            decimal1:         body["decimal1"],
            decimal2:         body["decimal2"],
            decimal3:         body["decimal3"],
            decimal4:         body["decimal4"],
            decimal5:         body["decimal5"],
            date1:            body["date1"],
            date2:            body["date2"],
            date3:            body["date3"],
            date4:            body["date4"],
            date5:            body["date5"]
          })
          result_body.save!
          count += 1
        end

        # JSONのbodyカウントとDB登録件数に差異があればエラー
        if data['body'].length != count
          raise "not match count"
        end

      end
      render status: 200, json: { jobno: jobno, count: count }

    rescue => e
      render status: 406, json: { message: e.message }

    end

  end

  private

  def getNextJobNo

    last_result_head = ResultHead.user_results(current_user).last

    # 何も登録されていない場合
    if last_result_head.nil?
      return Time.now.strftime('%y%m%d') << "0001"
    else
      last_jobno = last_result_head.jobno

      if last_jobno.slice(0, 6) === Time.now.strftime('%y%m%d')
        next_jobno = (last_jobno.to_i + 1).to_s
      else
        next_jobno = Time.now.strftime('%y%m%d') << "0001"
      end

      return next_jobno

    end

  end

end