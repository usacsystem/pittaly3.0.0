# coding: utf-8
class Api::V1::StaffsController < ApplicationController
  before_filter :authenticate_user!
  respond_to :json

  # ユーザの担当者一覧を出力する
  def index
    @staffs = Staff.user_staffs(current_user).order_by_id
    respond_with @staffs
  end
end
