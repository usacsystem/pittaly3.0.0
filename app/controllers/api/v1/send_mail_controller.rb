# coding: UTF-8

class Api::V1::SendMailController < ApplicationController
  def device_csv
    data   = ActiveSupport::JSON.decode(request.body)
    Api::V1::SendMail.device_csv(data).deliver

    filepath = "csv/" + data["uuid"].to_s + ".csv"

    if File::exists?(filepath)
      File::delete(filepath)
    end

    params = {
      send: true
    }
    render json: params
  end
end
