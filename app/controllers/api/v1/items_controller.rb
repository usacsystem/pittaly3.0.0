# coding: utf-8
class Api::V1::ItemsController < ApplicationController
  before_filter :authenticate_user!
  respond_to :json

  # ユーザの商品一覧を出力する
  def index
    start = params[:start]
    @items = Item.user_items(current_user).offset(start).limit(5000).order_by_id
    respond_with @items
  end

end
