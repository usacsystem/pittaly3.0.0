class Api::V1::SyncHistoriesController < ApplicationController
  respond_to :json

  # マスタ同期履歴の一覧を出力する
  def index
    @sync_histories = License.where('uuid = ?', params[:uuid]).first.user.sync_histories
    respond_with @sync_histories
  end
end
