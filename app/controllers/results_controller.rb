# coding: utf-8
class ResultsController < ApplicationController
  before_filter :authenticate_user!

  # ログインユーザの本日分のデータのみを出力する
  def index
    @results = ResultHead.user_results(current_user).today.order_jobno_desc
    @licenses = License.user_licenses(current_user)
    @information = Information.current_article.order_by_id_desc
  end

end
