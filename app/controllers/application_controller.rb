class ApplicationController < ActionController::Base
  protect_from_forgery

  def create_or_update_sync_history(cd)
    user_id = current_user.id

    @sync_history = SyncHistory.find_or_initialize_by_user_id_and_cd(user_id, cd)
    @sync_history.touch
    @sync_history.save!
  end

  def raise_save_error(model)
      model.errors[:base] << t('exception.save_error')
      render action: "new"
  end
end
