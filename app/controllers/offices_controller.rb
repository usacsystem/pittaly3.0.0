# coding: utf-8
class OfficesController < ApplicationController
  before_filter :authenticate_user!
  respond_to :html, :json

  # GET /offices
  def index
    gon.url = offices_url
    @offices =  Office.user_offices(current_user)
                .page(params[:page])
                .per(params[:per])
                .order(:cd)
    if params[:keyword]
      wc = "%"
      keyword = wc + params[:keyword] + wc
      @offices = @offices.where("name like ?", keyword)
    end
    respond_with @offices
  end

  # GET /offices/new
  def new
    @office = Office.user_new(current_user)
    respond_with @office
  end

  # GET /offices/1/edit
  def edit
    @office = Office.user_office(current_user, params[:id])
    # ユーザが保持するID以外を指定した場合は、一覧ページにリダイレクトさせる
    redirect_to offices_url, alert: t('controllers.offices.alert.find') and return if @office.blank?

    respond_with @office
  end

  # POST /offices
  def create
    @office = Office.user_office_create(current_user, params[:office])

    if params[:office][:cd].to_i <= 0
      @office.errors[:base] << t('controllers.offices.alert.cd_minus')
      render action: "new" and return
    end

    begin
      ActiveRecord::Base.transaction() do
        if @office.save
          create_or_update_sync_history(SYNC_HISTORY_OFFICE)
          redirect_to offices_url, notice: t('controllers.offices.notice.create')
        else
          render action: "new"
        end
      end
    rescue => e
      puts e.message
      raise_save_error(@office)
    end
  end

  # PUT /offices/1
  def update
    @office = Office.user_office(current_user, params[:id])

    if params[:office][:cd].to_i <= 0
      @office.errors[:base] << t('controllers.offices.alert.cd_minus')
      render action: "edit" and return
    end

    begin
      ActiveRecord::Base.transaction() do
        if @office.update_attributes(params[:office])
          create_or_update_sync_history(SYNC_HISTORY_OFFICE)
          redirect_to offices_url, notice: t('controllers.offices.notice.update')
        else
          render action: "edit"
        end
      end
    rescue => e
      puts e.message
      raise_save_error(@office)
    end
  end

  # DELETE /offices/1
  def destroy
    @office = Office.user_office(current_user, params[:id])

    begin
      ActiveRecord::Base.transaction() do
        @office.destroy
        create_or_update_sync_history(SYNC_HISTORY_OFFICE)
        redirect_to offices_url, notice: t('controllers.offices.notice.destroy')
      end
    rescue => e
      puts e.message
      redirect_to offices_url, notice: t('exception.delete_error')
    end
  end
end