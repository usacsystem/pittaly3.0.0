# coding: utf-8
class StaffsController < ApplicationController
  before_filter :authenticate_user!
  respond_to :html, :json

  # GET /staffs
  def index
    gon.url = staffs_url
    @staffs = Staff.user_staffs(current_user)
              .page(params[:page])
              .per(params[:per])
              .order(:cd)
    if params[:keyword]
      wc = "%"
      keyword = wc + params[:keyword] + wc
      @staffs = @staffs.where("name like ?", keyword)
    end
    respond_with @staffs
  end

  # GET /staffs/new
  def new
    @staff = Staff.user_new(current_user)
    respond_with @staff
  end

  # GET /staffs/1/edit
  def edit
    @staff = Staff.user_staff(current_user, params[:id])
    # ユーザが保持するID以外を指定した場合は、一覧ページにリダイレクトさせる
    redirect_to staffs_url, alert: t('controllers.staffs.alert.find') and return if @staff.blank?

    respond_with @staff
  end

  # POST /staffs
  def create
    @staff = Staff.user_staff_create(current_user, params[:staff])

    if params[:staff][:cd].to_i <= 0
      @staff.errors[:base] << t('controllers.staffs.alert.cd_minus')
      render action: "new" and return
    end

    begin
      ActiveRecord::Base.transaction() do
        if @staff.save
          create_or_update_sync_history(SYNC_HISTORY_STAFF)
          redirect_to staffs_url, notice: t('controllers.staffs.notice.create')
        else
          render action: "new"
        end
      end
    rescue => e
      puts e.message
      raise_save_error(@staff)
    end
  end

  # PUT /staffs/1
  def update
    @staff = Staff.user_staff(current_user, params[:id])

    if params[:staff][:cd].to_i <= 0
      @staff.errors[:base] << t('controllers.staffs.alert.cd_minus')
      render action: "edit" and return
    end

    begin
      ActiveRecord::Base.transaction() do
        if @staff.update_attributes(params[:staff])
          create_or_update_sync_history(SYNC_HISTORY_STAFF)
          redirect_to staffs_url, notice: t('controllers.staffs.notice.update')
        else
          render action: "edit"
        end
      end
    rescue => e
      puts e.message
      raise_save_error(@staff)
    end
  end

  # DELETE /staffs/1
  def destroy
    @staff = Staff.user_staff(current_user, params[:id])

    begin
      ActiveRecord::Base.transaction() do
        @staff.destroy
        create_or_update_sync_history(SYNC_HISTORY_STAFF)
        redirect_to staffs_url, notice: t('controllers.staffs.notice.destroy')
      end
    rescue => e
      puts e.message
      redirect_to staffs_url, notice: t('exception.delete_error')
    end
  end
end
