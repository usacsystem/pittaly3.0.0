# coding: utf-8
class CommonHeadsController < ApplicationController
  before_filter :authenticate_user!
  respond_to :html

  # GET /common_heads => /mylists
  def index
    gon.url = common_heads_url
    @common_heads = CommonHead.user_common_heads(current_user)
                    .page(params[:page])
                    .per(params[:per])
                    .order(:cd)
    if params[:keyword]
      wc = "%"
      keyword = wc + params[:keyword] + wc
      @common_heads = @common_heads.where("name like ?", keyword)
    end
    respond_with @common_heads
  end

  # GET /common_heads/1 => /mylists/1
  def show
    @common_head = CommonHead.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @common_head }
    end
  end

  # GET /common_heads/new => /mylist/new
  def new
    @common_head = CommonHead.user_new(current_user)
    respond_with @common_head
  end

  # GET /common_heads/1/edit => /mylist/1/edit
  def edit
    @common_head = CommonHead.user_common_head(current_user, params[:id])
    # ユーザが保持するID以外を指定した場合は、一覧ページにリダイレクトさせる
    redirect_to common_heads_url, alert: t('controllers.common_heads.alert.find') and return if @common_head.blank?

    respond_with @common_head
  end

  # POST /common_heads => /mylists
  def create
    if params[:common_head][:cd].to_i <= 0
      @common_head = CommonHead.user_new(current_user)
      @common_head.cd = params[:common_head][:cd]
      @common_head.name = params[:common_head][:name]
      if !params[:common_head][:common_bodies_attributes].nil?
        @common_head.common_bodies_attributes = params[:common_head][:common_bodies_attributes]
      end
      @common_head.errors[:base] << t('controllers.common_heads.alert.cd_minus')
      render action: "new" and return
    end

    if params[:common_head][:common_bodies_attributes].nil?
      @common_head = CommonHead.user_new(current_user)
      @common_head.cd = params[:common_head][:cd]
      @common_head.name = params[:common_head][:name]
      @common_head.errors[:base] << t('controllers.common_heads.alert.blank')
      render action: "new" and return
    end

    @common_head = CommonHead.user_common_head_create(current_user, params[:common_head])

    begin
      ActiveRecord::Base.transaction() do
        if @common_head.save
          create_or_update_sync_history(SYNC_HISTORY_MYLIST)
          redirect_to common_heads_url, notice: t('controllers.common_heads.notice.create')
        else
          render action: "new"
        end
      end
    rescue => e
      puts e.message
      raise_save_error(@common_head)
    end
  end

  # PUT /common_heads/1 => /mylists/1
  def update
    @common_head = CommonHead.user_common_head(current_user, params[:id])

    if params[:common_head][:cd].to_i <= 0
      @common_head.errors[:base] << t('controllers.common_heads.alert.cd_minus')
      render action: "edit" and return
    end

    begin
      ActiveRecord::Base.transaction() do
        if @common_head.update_attributes(params[:common_head])
          if @common_head.common_bodies.count == 0
            destroy and return
          end
          create_or_update_sync_history(SYNC_HISTORY_MYLIST)
          redirect_to common_heads_url, notice: t('controllers.common_heads.notice.update')
        else
          render action: "edit"
        end
      end
    rescue => e
      puts e.message
      raise_save_error(@common_head)
    end
  end

  # DELETE /common_heads/1 => /mylists/1
  def destroy
    @common_head = CommonHead.user_common_head(current_user, params[:id])

    begin
      ActiveRecord::Base.transaction() do
        @common_head.destroy
        create_or_update_sync_history(SYNC_HISTORY_MYLIST)
        redirect_to common_heads_url, notice: t('controllers.common_heads.notice.destroy')
      end
    rescue => e
      puts e.message
      redirect_to common_heads_url, notice: t('exception.delete_error')
    end
  end
end
