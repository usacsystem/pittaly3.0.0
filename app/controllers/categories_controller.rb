# coding: utf-8
class CategoriesController < ApplicationController
  before_filter :authenticate_user!
  respond_to :html, :json

  # GET /categories
  def index
    gon.url = categories_url
    @categories = Category.user_categories(current_user)
                  .page(params[:page])
                  .per(params[:per])
                  .order(:cd)
    if params[:keyword]
      wc = "%"
      keyword = wc + params[:keyword] + wc
      @categories = @categories.where("name like ?", keyword)
    end
    respond_with @categories
  end

  # GET /categories/new
  def new
    @category = Category.user_new(current_user)
    respond_with @category
  end

  # GET /categories/1/edit
  def edit
    @category = Category.user_category(current_user, params[:id])
    # ユーザが保持するID以外を指定した場合は、一覧ページにリダイレクトさせる
    redirect_to categories_url, alert: t('controllers.categories.alert.find') and return if @category.blank?

    respond_with @category
  end

  # POST /categories
  def create
    # @category = Category.user_category_create(current_user, params[:category])
    params[:category][:user_id] = current_user.id
    @category = Category.new(params[:category])

    if params[:category][:cd].to_i <= 0
      @category.errors[:base] << t('controllers.categories.alert.cd_minus')
      render action: "new" and return
    end

    begin
      ActiveRecord::Base.transaction() do
        if @category.save
          create_or_update_sync_history(SYNC_HISTORY_CATEGORY)
          redirect_to categories_url, notice: t('controllers.categories.notice.create')
        else
          render action: "new"
        end
      end
    rescue => e
      puts e.message
      raise_save_error(@category)
    end
  end

  # PUT /categories/1
  def update
    @category = Category.user_category(current_user, params[:id])

    if params[:category][:cd].to_i <= 0
      @category.errors[:base] << t('controllers.categories.alert.cd_minus')
      render action: "edit" and return
    end

    begin
      ActiveRecord::Base.transaction() do
        if @category.update_attributes(params[:category])
          create_or_update_sync_history(SYNC_HISTORY_CATEGORY)
          redirect_to categories_url, notice: t('controllers.categories.notice.update')
        else
          render action: "edit"
        end
      end
    rescue => e
      puts e.message
      raise_save_error(@category)
    end
  end

  # DELETE /categories/1
  def destroy
    @category = Category.user_category(current_user, params[:id])

    begin
      ActiveRecord::Base.transaction() do
        @category.destroy
        create_or_update_sync_history(SYNC_HISTORY_CATEGORY)
        redirect_to categories_url, notice: t('controllers.categories.notice.destroy')
      end
    rescue => e
      puts e.message
      redirect_to categories_url, notice: t('exception.delete_error')
    end
  end
end
