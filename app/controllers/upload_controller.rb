# coding: utf-8
class UploadController < ApplicationController
  before_filter :authenticate_user!

  require 'csv'

  def import_file

    # マスタの種類が選択され、ファイルも指定されている場合
    if params[:file].blank? == false && params[:page][:data].blank? == false

      # POSTパラメータを退避
      file = params[:file].tempfile
      data = params[:page][:data]

      # 取込件数カウント
      count = 0

      begin
        ActiveRecord::Base.transaction() do
          case data
            # 商品マスタの場合
            when "items"
              # ユーザの既存商品データは全て削除
              Item.user_items(current_user).delete_all

              # アップロードされたCSVを取り込む
              CSV.foreach(file, encoding: "CP932") do |row|
                Item.user_item_import_from_csv(current_user, row).save!
                count += 1
                if count == 1
                  create_or_update_sync_history(SYNC_HISTORY_ITEM)
                end
              end

            # 店舗マスタの場合
            when "offices"
              # ユーザの既存店舗データは全て削除
              Office.user_offices(current_user).delete_all

              # アップロードされたCSVを取り込む
              CSV.foreach(file, encoding: "CP932") do |row|
                Office.user_office_import_from_csv(current_user, row).save!
                count += 1
                if count == 1
                  create_or_update_sync_history(SYNC_HISTORY_OFFICE)
                end
              end

            # 担当者マスタの場合
            when "staffs"
              # ユーザの既存担当者データは全て削除
              Staff.user_staffs(current_user).delete_all

              # アップロードされたCSVを取り込む
              CSV.foreach(file, encoding: "CP932") do |row|
                Staff.user_staff_import_from_csv(current_user, row).save!
                count += 1

                if count == 1
                  create_or_update_sync_history(SYNC_HISTORY_STAFF)
                end
              end

            # マイリストマスタの場合
            when "mylists"
              # ユーザの既存のマイリストデータは全て削除
              CommonHead.user_common_heads(current_user).delete_all

              # アップロードされたCSVを取り込む
              old_cd = ""
              old_name = ""
              ch = nil
              # マイリストヘッダを更新する
              CSV.foreach(file, encoding: "CP932") do |row|
                if old_cd != row[0] || old_name != row[1]
                  ch = CommonHead.user_common_head_import_from_csv(current_user, row)
                  ch.save!
                  old_cd = row[0]
                  old_name = row[1]
                end

                # マイリスト明細を更新する
                cb = CommonBody.user_common_body_import_from_csv(ch, row)
                cb.save

                count += 1
                if count == 1
                  puts "********** SYNC MYLIST **********"
                  puts "count: " + count.to_s
                  puts "sync done"
                  create_or_update_sync_history(SYNC_HISTORY_MYLIST)
                else
                  puts "count: " + count.to_s
                  puts "not sync"
                end
              end
          end
        end
        # 正常に取り込めた場合
        redirect_to upload_url, notice: count.to_s << "件取り込みました。"
        # render status: 200, json: { count: count }
      rescue => e
        # 取込処理に異常が発生した場合
        # render status: 406, json: { message: e.message }
        redirect_to upload_url, alert: e.message
      end

    end

  end

end
