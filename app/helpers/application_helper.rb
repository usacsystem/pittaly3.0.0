# coding: utf-8
module ApplicationHelper
  def destroy_link_to(path)
    link_to t('helper.application.delete').html_safe, path,
      method: :delete,
      class: 'btn btn-danger',
      data: { confirm: t('helper.application.confirm') },
      'data-confirm-title' => '&nbsp;',
      'data-confirm-fade' => true,
      'data-confirm-cancel-class' => 'btn-cancel',
      'data-confirm-proceed-class' => 'btn-danger',
      'data-confirm-cancel' => t('helper.application.btn_cancel'),
      'data-confirm-proceed' => t('helper.application.btn_delete')
  end
end
