# coding : utf-8
module LicensesHelper

  def convert_icon(str)
    if str.upcase.include?("IPHONE")
      '<i class="icon-mobile-phone icon-large"></i>'
    elsif str.upcase.include?("IPAD")
      '<i class="icon-tablet icon-large"></i>'
    else str.upcase.include?("IPOD")
      '<i class="icon-mobile-phone icon-large"></i>'
    end
  end

end