# coding: UTF-8

class Api::V1::SendMail < ActionMailer::Base
  # default from: "from@example.com"

  def device_csv(data)
    to          = data["mail"]["to"]
    cc          = data["mail"]["cc"]
    bcc         = data["mail"]["bcc"]
    from        = data["mail"]["from"].to_s
    subject     = data["mail"]["subject"].to_s
    body_text   = data["mail"]["body_text"].to_s
    filename    = data["mail"]["filename"].to_s
    uuid        = data["uuid"].to_s
    head        = data["head"]
    body        = data["head"]["body"]

    puts "**** 送り先 ****"
    puts "uuid: " + uuid.to_s
    puts "to1: " + to[0].to_s + "  to2: " + to[1].to_s
    puts "cc1: " + cc[0].to_s + "  cc2: " + cc[1].to_s
    puts "bcc1: " + bcc[0].to_s + "  bcc2: " + bcc[1].to_s
    puts "from: " + from
    puts "subject: " + subject
    puts "body_text: " + body_text
    puts "**** メール内容 ****"
    puts "subject: " + subject
    puts body_text
    puts "filename: " + filename
    puts "**** ヘッダ ****"
    puts head
    puts "**** 明細 ****"
    puts body

    # 必要な情報がなければ書き換え
    if from == ""
      from = t("controllers.change.from")
    end
    if subject == ""
      subject = t("controllers.change.subject")
    end
    if filename == ""
      filename = t("controllers.change.filename")
    end

    # ヘッダ、明細の内容をcsvに書き出し
    tmp_filepath = "csv/" + uuid + ".csv"
    write_data(tmp_filepath, head, body)

    attachments[filename] = {
      content: File.binread("csv/" + uuid + ".csv"),
      transfer_encoding: :binary
    }

    # 上で書き出したファイルを、名前を変えてメール送信
    @body_text = body_text

    mail  from:         from,
      to:           to,
      cc:           cc,
      bcc:          bcc,
      subject:      subject
  end

  private
  def write_data(path, head, body)
    # File.open(tmp_filepath, "w:Shift_JIS:UTF-8") do |f|
    #   f.write "test1" + "\r\n"
    #   f.write "test2" + "\r\n"
    #   f.write "test3" + "\r\n"
    # end
    require 'csv'
    require 'kconv'

    csv_data = CSV.generate(force_quotes: true, row_sep: "\r\n") do |csv|
      csv << [
        I18n.t("csv_item.result_date"),
        I18n.t("csv_item.result_time"),
        I18n.t("activerecord.attributes.result_head.categorycd"),
        I18n.t("activerecord.attributes.category.name"),
        I18n.t("activerecord.attributes.result_head.officecd"),
        I18n.t("activerecord.attributes.office.name"),
        I18n.t("activerecord.attributes.result_head.staffcd"),
        I18n.t("activerecord.attributes.staff.name"),
        I18n.t("activerecord.attributes.result_head.jobno"),
        I18n.t("activerecord.attributes.result_head.string1"),
        I18n.t("activerecord.attributes.result_head.string2"),
        I18n.t("activerecord.attributes.result_head.string3"),
        I18n.t("activerecord.attributes.result_head.string4"),
        I18n.t("activerecord.attributes.result_head.string5"),
        I18n.t("activerecord.attributes.result_head.decimal1"),
        I18n.t("activerecord.attributes.result_head.decimal2"),
        I18n.t("activerecord.attributes.result_head.decimal3"),
        I18n.t("activerecord.attributes.result_head.decimal4"),
        I18n.t("activerecord.attributes.result_head.decimal5"),
        I18n.t("activerecord.attributes.result_head.date1"),
        I18n.t("activerecord.attributes.result_head.date2"),
        I18n.t("activerecord.attributes.result_head.date3"),
        I18n.t("activerecord.attributes.result_head.date4"),
        I18n.t("activerecord.attributes.result_head.date5"),
        I18n.t("activerecord.attributes.result_body.row"),
        I18n.t("activerecord.attributes.result_body.skucd"),
        I18n.t("activerecord.attributes.item.jancd"),
        I18n.t("activerecord.attributes.item.name"),
        I18n.t("activerecord.attributes.item.kind"),
        I18n.t("activerecord.attributes.item.price"),
        I18n.t("activerecord.attributes.item.carton"),
        I18n.t("activerecord.attributes.result_body.quantity"),
        I18n.t("activerecord.attributes.result_body.string1"),
        I18n.t("activerecord.attributes.result_body.string2"),
        I18n.t("activerecord.attributes.result_body.string3"),
        I18n.t("activerecord.attributes.result_body.string4"),
        I18n.t("activerecord.attributes.result_body.string5"),
        I18n.t("activerecord.attributes.result_body.decimal1"),
        I18n.t("activerecord.attributes.result_body.decimal2"),
        I18n.t("activerecord.attributes.result_body.decimal3"),
        I18n.t("activerecord.attributes.result_body.decimal4"),
        I18n.t("activerecord.attributes.result_body.decimal5"),
        I18n.t("activerecord.attributes.result_body.date1"),
        I18n.t("activerecord.attributes.result_body.date2"),
        I18n.t("activerecord.attributes.result_body.date3"),
        I18n.t("activerecord.attributes.result_body.date4"),
        I18n.t("activerecord.attributes.result_body.date5")
      ]

      h = head
      # h_modify_date = h["modify_date"].blank? ? "" : h["modify_date"].strftime('%Y%m%d')
      # h_modify_time = h["modify_date"].blank? ? "" : h["modify_date"].strftime('%H%M%S')
      # h_date1       = h["date1"].blank?       ? "" : h["date1"].strftime('%Y%m%d')
      # h_date2       = h["date2"].blank?       ? "" : h["date2"].strftime('%Y%m%d')
      # h_date3       = h["date3"].blank?       ? "" : h["date3"].strftime('%Y%m%d')
      # h_date4       = h["date4"].blank?       ? "" : h["date4"].strftime('%Y%m%d')
      # h_date5       = h["date5"].blank?       ? "" : h["date5"].strftime('%Y%m%d')

      body.each do |b|
        # b_date1       = b["date1"].blank?       ? "" : b["date1"].strftime('%Y%m%d')
        # b_date2       = b["date2"].blank?       ? "" : b["date2"].strftime('%Y%m%d')
        # b_date3       = b["date3"].blank?       ? "" : b["date3"].strftime('%Y%m%d')
        # b_date4       = b["date4"].blank?       ? "" : b["date4"].strftime('%Y%m%d')
        # b_date5       = b["date5"].blank?       ? "" : b["date5"].strftime('%Y%m%d')

        csv << [
          h["modify_date"],
          h["modify_time"],
          h["category_cd"],
          h["category_name"],
          h["office_cd"],
          h["office_name"],
          h["staff_cd"],
          h["staff_name"],
          h["jobno"],
          h["string1"],
          h["string2"],
          h["string3"],
          h["string4"],
          h["string5"],
          h["decimal1"],
          h["decimal2"],
          h["decimal3"],
          h["decimal4"],
          h["decimal5"],
          h["date1"],
          h["date2"],
          h["date3"],
          h["date4"],
          h["date5"],
          b["row"],
          b["skucd"],
          b["jancd"],
          b["name"],
          b["kind"],
          b["price"],
          b["carton"],
          b["quantity"],
          b["string1"],
          b["string2"],
          b["string3"],
          b["string4"],
          b["string5"],
          b["decimal1"],
          b["decimal2"],
          b["decimal3"],
          b["decimal4"],
          b["decimal5"],
          b["date1"],
          b["date2"],
          b["date3"],
          b["date4"],
          b["date5"]
        ]
      end
    end
    csv_data = csv_data.kconv(Kconv::SJIS,Kconv::UTF8)

    File.open(path, "w:Shift_JIS:UTF-8") do |f|
      f.write csv_data
    end
  end
end
