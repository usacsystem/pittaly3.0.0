$(document).ready(function(){
  $(".page").bind("change", function(){
    var per = $(this).val();
    var keyword = $(this).parent().next().find("#keyword").val();
    openUrl(per, keyword);
  });

  $("#search").click(function(){
    var per = $(this).parent().parent().prev().find(".page").val();
    var keyword = $(this).prev().val();
    openUrl(per, keyword);
  });
});

function openUrl(_per, _keyword) {
  var url = gon.url + "?page=1&per=" + _per;
  if (_keyword !== "") {
    url += "&keyword=" + encodeURI(_keyword);
  }
  if (url) {
    window.location.replace(url);
  }
}
