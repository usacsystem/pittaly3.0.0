$('#datatable').dataTable({
  "sDom":             "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
  "sPaginationType":  "bootstrap",
  "oLanguage": {
    "sLengthMenu":    "表示件数 _MENU_ 件",
    "oPaginate": {
      "sNext":        "次へ",
      "sPrevious":    "前へ"
    },
    "sInfo":          "<strong>全_TOTAL_件中 _START_〜_END_件を表示</strong>",
    "sSearch":        "検索：",
    "sZeroRecords":   "データが見つかりません",
    "sInfoEmpty":     "データが見つかりません",
    "sInfoFiltered":  "(全_MAX_件より、フィルタリング)",
    "sProcessing":    "しばらくお待ちください…"
  },
  "bPaginate":        true,
  "bProcessing":      true,
  "aoColumns": [
    null,     // 商品コード
    null,     // ＪＡＮコード
    null,     // 商品名称
    null,     // 商品分類
    null,     // 商品価格
    null,     // 商品入数
    {"bSortable": false},     // 編集
    {"bSortable": false}     // 削除
  ]
});
