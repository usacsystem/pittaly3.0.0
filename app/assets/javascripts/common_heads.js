$(function() {
  // 初期化
  $("#item_cd").val("");
  $("#item_cd").attr("placeholder", "商品コード");
  $("#is_jan").prop("checked", false);
  $('input#item_skucd').val("");
  $('input#item_id').val("");
  $('input#item_jancd').val("");
  $('input#item_name').val("");
  $('input#item_kind').val("");
  $('input#item_price').val("");
  $('input#item_carton').val("");

  var disable_func = function() { return false; };
  disable_add_item();

  function disable_add_item() {
    $('#add_item').bind('click', disable_func);
    $('#add_item').attr('disabled', 'disabled');
  }
  function enable_add_item() {
    $('#add_item').unbind('click', disable_func);
    $('#add_item').removeAttr('disabled');
  }

  // 品番（skucd）で商品を検索し、商品IDと商品名称をセットする
  $('button#item_search').bind('click', function() {
    var skucd  = $('input#item_cd').val(),
        is_jan = $("#is_jan").prop("checked");

    if (skucd.length === 0) return;
    $.ajax({
      url:        '/items/search',
      dataType:   'json',
      data: {
        skucd:    skucd,
        is_jan:   is_jan
      },
      success:    function(data) {
        if (data.message === "not found") {
          disable_add_item();
          $('input#item_skucd').val('');
          $('input#item_id').val('');
          $('input#item_jancd').val('');
          $('input#item_name').val('');
          $('input#item_kind').val('');
          $('input#item_price').val('');
          $('input#item_carton').val('');
          return;
        }
        // 現在のテーブルチェック
        var flg = 0;
        if ($('tbody > tr > td > input').length === 0) enable_add_item();
        $('tbody > tr > td > input').each(function() {
          if ($(this)[0].value === data.skucd) {
            flg = 1;
          }
          // 既に商品が登録されている場合は、登録を許可しない
          console.log(flg);
          if (flg === 1) {
            disable_add_item();
          } else {
          // 登録されていない場合は、登録を許可する
            enable_add_item();
          }
        });
        $("#item_cd").val("");
        $('input#item_id').val(data.id);
        $('input#item_skucd').val(data.skucd);
        $('input#item_jancd').val(data.jancd);
        $('input#item_name').val(data.name);
        $('input#item_kind').val(data.kind);
        $('input#item_price').val(data.price);
        $('input#item_carton').val(data.carton);
      },
      error:      function(data) {}
    });
  });

  // nested_formの明細追加処理
  $(document).on('nested:fieldAdded:common_bodies', function(e) {
    console.log(e);

    var item_skucd_field = e.field.find('.item_skucd');
    var item_jancd_field = e.field.find('.item_jancd');
    var item_name_field = e.field.find('.item_name');
    var item_kind_field = e.field.find('.item_kind');
    var item_price_field = e.field.find('.item_price');
    var item_carton_field = e.field.find('.item_carton');

    item_skucd_field.val($('input#item_skucd').val());
    item_jancd_field.text($('input#item_jancd').val());
    item_name_field.text($('input#item_name').val());
    item_kind_field.text($('input#item_kind').val());
    item_price_field.text($('input#item_price').val());
    item_carton_field.text($('input#item_carton').val());

    $('input#item_skucd').val('');
    $('input#item_id').val('');
    $('input#item_jancd').val('');
    $('input#item_name').val('');
    $('input#item_kind').val('');
    $('input#item_price').val('');
    $('input#item_carton').val('');

    disable_add_item();
  });

  $("#is_jan").on("change", function(){
    var is_jan = $("#is_jan"),
        skucd  = $("#item_cd");

    if (is_jan.prop("checked")) {
      skucd.attr("placeholder", "バーコード");
    } else {
      skucd.attr("placeholder", "商品コード");
    }
  });
});

$('#datatable').dataTable({
  "sDom":             "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
  "sPaginationType":  "bootstrap",
  "oLanguage": {
    "sLengthMenu":    "表示件数 _MENU_ 件",
    "oPaginate": {
      "sNext":        "次へ",
      "sPrevious":    "前へ"
    },
    "sInfo":          "<strong>全_TOTAL_件中 _START_〜_END_件を表示</strong>",
    "sSearch":        "検索：",
    "sZeroRecords":   "データが見つかりません",
    "sInfoEmpty":     "データが見つかりません",
    "sInfoFiltered":  "(全_MAX_件より、フィルタリング)",
    "sProcessing":    "しばらくお待ちください…"
  },
  "bPaginate":        true,
  "bProcessing":      true,
  "aoColumns": [
    null,     // マイリストコード
    null,     // マイリスト名称
    null,     // 登録商品明細件数
    {"bSortable": false},     // 編集
    {"bSortable": false}     // 削除
  ]
});