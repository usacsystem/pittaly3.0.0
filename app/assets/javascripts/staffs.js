$('#datatable').dataTable({
  "sDom":             "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
  "sPaginationType":  "bootstrap",
  "oLanguage": {
    "sLengthMenu":    "表示件数 _MENU_ 件",
    "oPaginate": {
      "sNext":        "次へ",
      "sPrevious":    "前へ"
    },
    "sInfo":          "<strong>全_TOTAL_件中 _START_〜_END_件を表示</strong>",
    "sSearch":        "検索：",
    "sZeroRecords":   "データが見つかりません",
    "sInfoEmpty":     "データが見つかりません",
    "sInfoFiltered":  "(全_MAX_件より、フィルタリング)",
    "sProcessing":    "しばらくお待ちください…"
  },
  "bPaginate":        true,
  "bProcessing":      true,
  "aoColumns": [
    null,     // 担当者コード
    null,     // 担当者名称
    null,     // 店舗コード
    {"bSortable": false},     // 編集
    {"bSortable": false}     // 削除
  ]
});

$(document).ready(function(){
  $("button.search").on("click", function(){
    $.ajax({
      url: "/offices",
      type: "GET",
      dataType: "json",
      success: function(json){
        if(json.length <= 0) {
          alert("データがありません。");
          return;
        }

        $("table#modal-table > tbody").empty();

        var str = "";
        $.each(json, function(i, e){
          str = "<tr>" +
                "<td>" + e.cd +   "</td>" +
                "<td>" + e.name + "</td>" +
                "<td><button id='" + e.cd + "' class='btn btn-primary action modal_select'>選択</button></td>" +
                "</tr>";
          $("table#modal-table > tbody").append(str);
        });

        $("button.action").on("click", function(e){
          var ret_cd   = $(this).parent().prev().prev().text();
          $("#staff_office_cd").val(ret_cd);
          $("#modal_search").modal("hide");
        });

        $("#modal_search").modal("show");
      },
    });
  });
});