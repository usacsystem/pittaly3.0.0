$('.activate_license').bind('ajax:success', function(data, status, xhr) {
  if (status.update_activate_license === true) {
    switch (status.activation) {
      case true:
        $(this).parent().parent().removeClass('error');
        break;
      case false:
        $(this).parent().parent().addClass('error');
        break;
    }
  } else {
    // message
  }
});

$('#datatable').dataTable({
  "sDom":             "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
  "sPaginationType":  "bootstrap",
  "oLanguage": {
    "sLengthMenu":    "表示件数 _MENU_ 件",
    "oPaginate": {
      "sNext":        "次へ",
      "sPrevious":    "前へ"
    },
    "sInfo":          "<strong>全_TOTAL_件中 _START_〜_END_件を表示</strong>",
    "sSearch":        "検索：",
    "sZeroRecords":   "データが見つかりません",
    "sInfoEmpty":     "データが見つかりません",
    "sInfoFiltered":  "(全_MAX_件より、フィルタリング)",
    "sProcessing":    "しばらくお待ちください…"
  },
  "bPaginate":        true,
  "bProcessing":      true,
  "aoColumns": [
    null,     // ライセンス管理名称
    null,     // 端末認証アカウント
    null,     // 端末認証コード
    null,     // 登録日時
    null,     // 認証された端末
    {"bSortable": false}
  ]
});