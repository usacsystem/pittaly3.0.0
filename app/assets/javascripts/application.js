//= require jquery
//= require jquery_ujs
//= require jquery.ui.core
//= require jquery.ui.datepicker
//= require jquery.ui.datepicker-ja
//= require jquery_nested_form
//= require bootstrap-transition
//= require bootstrap-affix
//= require bootstrap-alert
//= require bootstrap-button
//= require bootstrap-carousel
//= require bootstrap-collapse
//= require bootstrap-dropdown-fix
//= require bootstrap-modal
//= require bootstrap-scrollspy
//= require bootstrap-tab
//= require bootstrap-tooltip
//= require bootstrap-popover
//= require bootstrap-typeahead
//= require bootstrap-switch
//= require twitter/bootstrap/rails/confirm
//= require dataTables/jquery.dataTables
//= require dataTables/jquery.dataTables.bootstrap
//= require search_model

// 自動スクロール
$(".top").click(function () {
  $('html,body').animate({ scrollTop: 0 }, 'fast');
  return false;
});
