$('form').bind('ajax:success', function(data, status, xhr) {
  $('#search_result').html(status);

  $('#datatable').dataTable({
    "sDom":             "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
    "sPaginationType":  "bootstrap",
    "oLanguage": {
      "sLengthMenu":    "表示件数 _MENU_ 件",
      "oPaginate": {
        "sNext":        "次へ",
        "sPrevious":    "前へ"
      },
      "sInfo":          "<strong>全_TOTAL_件中 _START_〜_END_件を表示</strong>",
      "sSearch":        "検索：",
      "sZeroRecords":   "データが見つかりません",
      "sInfoEmpty":     "データが見つかりません",
      "sInfoFiltered":  "(全_MAX_件より、フィルタリング)",
      "sProcessing":    "しばらくお待ちください…"
    },
    "bPaginate":        true,
    "bProcessing":      true,
    // "bFilter":          false,
    "aoColumns": [
      null,     // 連番
      null,     // ジョブNo
      null,     // 処理費付け
      null,     // データ区分コード
      null,     // データ区分名称
      null,     // 店舗コード
      null,     // 店舗名称
      null,     // 担当者コード
      null,     // 担当者名称
      null,     // 明細件数
      null      // ダウンロード
    ]
  });
});

$('#search').bind('click', function() {
  $('.collapse').collapse('hide');
});

$('#cancel').bind('click', function() {
  $('input').val('');
  $('#export').bootstrapSwitch('setState', false);
  $('#search_result').empty();
});

var getModel = function( _params ) {
  $.ajax({
      url:      _params.path,
      type:     _params.type,
      dataType: _params.dataType,
      success:  function(json){
        // json null check
        if (json.length <= 0 ) {
          alert("データがありません。");
          return;
        }

        $("table#modal-table > tbody").empty();

        var str = "";
        // json.forEach(function (e, i) {
        jQuery.each(json, function (i, e) {
          str = "<tr>" +
                "<td>" + e.cd +"</td>" +
                "<td>" + e.name + "</td>" +
                "<td><button id='" + e.id + "' class='btn btn-primary action modal_select'>選択</button></td>" +
                "</tr>";
          $("table#modal-table > tbody").append(str);
        });

        $("button.action").on("click", function (e) {
          var ret_id = $(this).parent().prev().prev().text();
          $(_params.target).val(ret_id);
          $("#modal_search").modal("hide");
        });

        $('#modal_search').modal("show");
    }
  });
};


$(document).ready(function(){
    // カレンダー
  $( ".datepicker" ).datepicker({
    dateFormat: 'yy/mm/dd',
    showButtonPanel: true
  });

  $("button.search").on("click", function(e){
    var model     = "",
        tagName   = e.currentTarget.parentElement.children[0].tagName,
        id        = e.currentTarget.parentElement.children[0].id;

    if (id.indexOf("category") >= 0) {
      model = "categories";
    } else if (id.indexOf("office") >= 0){
      model = "offices";
    } else if (id.indexOf("staff") >= 0) {
      model = "staffs";
    }

    var params = {
      path:       model,
      type:       "GET",
      dataType:   "json",
      target:     tagName + "#" + id
    };

    getModel(params);
  });
});
