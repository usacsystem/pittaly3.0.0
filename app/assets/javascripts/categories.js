$('#datatable').dataTable({
  "sDom":             "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
  "sPaginationType":  "bootstrap",
  "oLanguage": {
    "sLengthMenu":    "表示件数 _MENU_ 件",
    "oPaginate": {
      "sNext":        "次へ",
      "sPrevious":    "前へ"
    },
    "sInfo":          "<strong>全_TOTAL_件中 _START_〜_END_件を表示</strong>",
    "sSearch":        "検索：",
    "sZeroRecords":   "データが見つかりません",
    "sInfoEmpty":     "データが見つかりません",
    "sInfoFiltered":  "(全_MAX_件より、フィルタリング)",
    "sProcessing":    "しばらくお待ちください…"
  },
  "bPaginate":        true,
  "bProcessing":      true,
  "aoColumns": [
    null,     // データ区分コード
    null,     // データ区分名称
    {"bSortable": false},     // 編集
    {"bSortable": false}     // 削除
  ]
});

$(document).ready(function(){
  // =======================
  // 追加ボタンクリックを無効化
  // =======================
  var add_head_string  = $("#add_head_string"),
      add_head_decimal = $("#add_head_decimal"),
      add_head_date    = $("#add_head_date"),
      add_body_string  = $("#add_body_string"),
      add_body_decimal = $("#add_body_decimal"),
      add_body_date    = $("#add_body_date");

  disable_add_customer_events(add_head_string);
  disable_add_customer_events(add_head_decimal);
  disable_add_customer_events(add_head_date);
  disable_add_customer_events(add_body_string);
  disable_add_customer_events(add_body_decimal);
  disable_add_customer_events(add_body_date);

  // =======================
  // 入力値のチェック
  // =======================
  $("#head_string_hint").bind("keydown keyup keypress change", function(){
    checkHints(this, "head_string");
  });

  $("#head_decimal_hint").bind("keydown keyup keypress change", function(){
    checkHints(this, "head_decimal");
  });

  $("#head_date_hint").bind("keydown keyup keypress change", function(){
    checkHints(this, "head_date");
  });

  $("#body_string_hint").bind("keydown keyup keypress change", function(){
    checkHints(this, "body_string");
  });

  $("#body_decimal_hint").bind("keydown keyup keypress change", function(){
    checkHints(this, "body_decimal");
  });

  $("#body_date_hint").bind("keydown keyup keypress change", function(){
    checkHints(this, "body_date");
  });

  // =======================
  // Nested_Formの明細追加処理
  // =======================
  $(document).on('nested:fieldAdded:head_strings', function(e) {
    setNestedForm(e, "head_string");
  });
  $(document).on('nested:fieldRemoved:head_strings', function(e) {
  });

  $(document).on('nested:fieldAdded:head_decimals', function(e) {
    setNestedForm(e, "head_decimal");
  });
  $(document).on('nested:fieldRemoved:head_decimals', function(e) {
  });

  $(document).on('nested:fieldAdded:head_dates', function(e) {
    setNestedForm(e, "head_date");
  });
  $(document).on('nested:fieldRemoved:head_dates', function(e) {
  });

  $(document).on('nested:fieldAdded:body_strings', function(e) {
    setNestedForm(e, "body_string");
  });
  $(document).on('nested:fieldRemoved:body_strings', function(e) {
  });

  $(document).on('nested:fieldAdded:body_decimals', function(e) {
    setNestedForm(e, "body_decimal");
  });
  $(document).on('nested:fieldRemoved:body_decimals', function(e) {
  });

  $(document).on('nested:fieldAdded:body_dates', function(e) {
    setNestedForm(e, "body_date");
  });
  $(document).on('nested:fieldRemoved:body_dates', function(e) {
  });
});

function setNestedForm(_row, _kind) {
  var set_hint    = _row.field.find(".hint"),
      get_hint    = $("#" + _kind + "_hint"),
      button      = $("#add_" + _kind);

  set_hint.val(get_hint.val());

  // セットしたら入力枠は初期化
  get_hint.val("");
  disable_add_customer_events(button);
}

function checkHints(_hint, _kind) {
  var length = $(_hint).val().replace(/(^\s+)|(\s+$)/g, "").length,
      button = $("#add_" + _kind),
      memo   = $("#" + _kind + "_memo"),
      hints  = $("tbody." + _kind + "s").children("tr[style!='display: none;']");

  if (length === 0) {
    disable_add_customer_events(button);
  } else {
    if (hints.length < 5) {
      memo.text("");
      enable_add_customer_events(button);
    } else {
      memo.text("５つ以上登録できません");
      disable_add_customer_events(button);
    }
  }
}

// nested_form_addボタンの無効化処理
function disable_func() { return false; }
function disable_add_customer_events(_button){
  _button.bind('click', disable_func).attr('disabled', 'disabled');
}
function enable_add_customer_events(_button){
  _button.unbind('click', disable_func).removeAttr('disabled');
}

