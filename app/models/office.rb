# coding: utf-8
class Office < ActiveRecord::Base
  # ===============
  # Mass Assignment
  # ===============
  attr_accessible :user_id, :cd, :name

  # ===========
  # Association
  # ===========
  belongs_to  :user
  has_many    :result_heads

  # =========
  # Validates
  # =========
  validates :cd,    presence: true,   uniqueness: { scope: :user_id }
  validates :name,  presence: true

  # =====
  # Scope
  # =====
  scope :user_offices,  lambda { |user| where('user_id = ?', user.id) }
  scope :order_by_id,   lambda { order('id') }
  scope :find_by_cd,    lambda { |user_id, cd| where("user_id = ? and cd = ?", user_id, cd) }

  # ============
  # Class Method
  # ============
  def self.user_office(user, id)
    where('user_id = ? and id = ?', user.id, id).first
  end

  def self.user_new(user)
    new({user_id: user.id})
  end

  def self.user_office_create(user, params)
    new({
      user_id:  user.id,
      cd:       params[:cd],
      name:     params[:name]
    })
  end

  def self.user_office_import_from_csv(user, row)
    new({
      user_id:  user.id,
      cd:       row[0],
      name:     row[1]
    })
  end
end
