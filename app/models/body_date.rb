class BodyDate < Preparation
  attr_accessible :type, :user_id, :category_id, :hint

  belongs_to :category

  # =====
  # Scope
  # =====
    scope :find_by_category,    lambda{|category_id| where("category_id = ?", category_id)}
    scope :order_by_id,         lambda { order('id') }
end
