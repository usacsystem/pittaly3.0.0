# coding: utf-8
class ResultBody < ActiveRecord::Base
  # ===============
  # Mass Assignment
  # ===============
  attr_accessible :result_head_id, :result_time, :skucd, :row, :quantity,
                  :string1,  :string2,  :string3,  :string4,  :string5,
                  :decimal1, :decimal2, :decimal3, :decimal4, :decimal5,
                  :date1,    :date2,    :date3,    :date4,    :date5

  # ===========
  # Association
  # ===========
  belongs_to :result_head
  belongs_to :item,   foreign_key: :skucd, primary_key: :skucd, conditions: proc { ['user_id = ?', self.result_head.user_id] }

  # =========
  # Validates
  # =========
  validates :row,       presence: true
  validates :quantity,  presence: true

  # =====
  # Scope
  # =====

  # ============
  # Class Method
  # ============


end
