# coding: utf-8
class ResultHead < ActiveRecord::Base
  # ===============
  # Mass Assignment
  # ===============
  attr_accessible :user_id, :license_id, :jobno, :categorycd, :officecd, :staffcd, :result_date, :export,
                  :string1,  :string2,  :string3,  :string4,  :string5,
                  :decimal1, :decimal2, :decimal3, :decimal4, :decimal5,
                  :date1,    :date2,    :date3,    :date4,    :date5
  # ===========
  # Association
  # ===========
  belongs_to :user
  # belongs_to :license,        conditions: proc { ['user_id = ?', self.user_id] }
  belongs_to :license
  has_many   :result_bodies,  dependent: :delete_all
  # ユーザIDを条件に含める
  belongs_to :category,       foreign_key: :categorycd, primary_key: :cd, conditions: proc { ['user_id = ?', self.user_id] }
  belongs_to :office,         foreign_key: :officecd,   primary_key: :cd, conditions: proc { ['user_id = ?', self.user_id] }
  belongs_to :staff,          foreign_key: :staffcd,    primary_key: :cd, conditions: proc { ['user_id = ?', self.user_id] }

  # =========
  # Validates
  # =========
  validates :user_id,     presence: true, uniqueness: { scope: :jobno }
  validates :license_id,  presence: true
  validates :jobno,       presence: true
  validates :result_date, presence: true
  validates :categorycd,  presence: true
  validates :officecd,    presence: true
  validates :staffcd,     presence: true
  validates :export,      presence: true
  validate :only_one_jobno_per_user

  # =====
  # Scope
  # =====
  scope :user_results,        lambda { |user| where('user_id = ?', user.id) }
  scope :today,               lambda { where('jobno like ?', Time.now.strftime('%y%m%d') + '%') }
  scope :where_categorycd,    lambda { |categorycd| where('categorycd = ?', categorycd) }
  scope :order_jobno_desc,    lambda { order('jobno desc') }
  scope :from_result_date,    lambda { |from| where('result_date >= ?', from) }
  scope :to_result_date,      lambda { |to| where('result_date <= ?', to) }
  scope :from_jobno,          lambda { |from| where('jobno >= ?', from) }
  scope :to_jobno,            lambda { |to| where('jobno <= ?', to) }
  scope :from_categorycd,     lambda { |from| where('categorycd >= ?', from) }
  scope :to_categorycd,       lambda { |to| where('categorycd <= ?', to) }
  scope :from_officecd,       lambda { |from| where('officecd >= ?', from) }
  scope :to_officecd,         lambda { |to| where('officecd <= ?', to) }
  scope :from_staffcd,        lambda { |from| where('staffcd >= ?', from) }
  scope :to_staffcd,          lambda { |to| where('staffcd <= ?', to) }
  scope :done_export,         lambda { |export| where('export = ?', export) }

  # ============
  # Class Method
  # ============
  # 同一ユーザでjobnoの重複を禁止する
  def only_one_jobno_per_user
    if ResultHead.exists?(user_id: user_id, jobno: jobno)
      errors.add("Error! There is a jobno post for this user!")
    end
  end

  # 実績照会の検索
  def self.search_results(user, params)
    results = self.user_results(user)

    if !params[:from_result_date].blank?
      results = results.from_result_date(Date.parse(params[:from_result_date]).strftime('%Y-%m-%d'))
    end
    if !params[:to_result_date].blank?
      results = results.to_result_date(Date.parse(params[:to_result_date]).strftime('%Y-%m-%d'))
    end

    if !params[:from_jobno].blank?
      results = results.from_jobno(params[:from_jobno].to_s)
    end
    if !params[:to_jobno].blank?
      results = results.to_jobno(params[:to_jobno].to_s)
    end

    if !params[:from_categorycd].blank?
      results = results.from_categorycd(params[:from_categorycd].to_i)
    end
    if !params[:to_categorycd].blank?
      results = results.to_categorycd(params[:to_categorycd].to_i)
    end

    if !params[:from_officecd].blank?
      results = results.from_officecd(params[:from_officecd].to_i)
    end
    if !params[:to_officecd].blank?
      results = results.to_officecd(params[:to_officecd].to_i)
    end

    if !params[:from_staffcd].blank?
      results = results.from_staffcd(params[:from_staffcd].to_i)
    end
    if !params[:to_staffcd].blank?
      results = results.to_staffcd(params[:to_staffcd].to_i)
    end

    if params[:export].nil?
      results = results.done_export(false)
    else
      results = results.done_export(true)
    end

    return results
  end

  # CSV出力
  def self.csv(user, params)
    require 'csv'
    require 'kconv'

    csv_data = CSV.generate(force_quotes: true, row_sep: "\r\n") do |csv|
      csv << [
        I18n.t("csv_item.result_date"),
        I18n.t("csv_item.result_time"),
        I18n.t("activerecord.attributes.result_head.categorycd"),
        I18n.t("activerecord.attributes.category.name"),
        I18n.t("activerecord.attributes.result_head.officecd"),
        I18n.t("activerecord.attributes.office.name"),
        I18n.t("activerecord.attributes.result_head.staffcd"),
        I18n.t("activerecord.attributes.staff.name"),
        I18n.t("activerecord.attributes.result_head.jobno"),
        I18n.t("activerecord.attributes.result_head.string1"),
        I18n.t("activerecord.attributes.result_head.string2"),
        I18n.t("activerecord.attributes.result_head.string3"),
        I18n.t("activerecord.attributes.result_head.string4"),
        I18n.t("activerecord.attributes.result_head.string5"),
        I18n.t("activerecord.attributes.result_head.decimal1"),
        I18n.t("activerecord.attributes.result_head.decimal2"),
        I18n.t("activerecord.attributes.result_head.decimal3"),
        I18n.t("activerecord.attributes.result_head.decimal4"),
        I18n.t("activerecord.attributes.result_head.decimal5"),
        I18n.t("activerecord.attributes.result_head.date1"),
        I18n.t("activerecord.attributes.result_head.date2"),
        I18n.t("activerecord.attributes.result_head.date3"),
        I18n.t("activerecord.attributes.result_head.date4"),
        I18n.t("activerecord.attributes.result_head.date5"),
        I18n.t("activerecord.attributes.result_body.row"),
        I18n.t("activerecord.attributes.result_body.skucd"),
        I18n.t("activerecord.attributes.item.jancd"),
        I18n.t("activerecord.attributes.item.name"),
        I18n.t("activerecord.attributes.item.kind"),
        I18n.t("activerecord.attributes.item.price"),
        I18n.t("activerecord.attributes.item.carton"),
        I18n.t("activerecord.attributes.result_body.quantity"),
        I18n.t("activerecord.attributes.result_body.string1"),
        I18n.t("activerecord.attributes.result_body.string2"),
        I18n.t("activerecord.attributes.result_body.string3"),
        I18n.t("activerecord.attributes.result_body.string4"),
        I18n.t("activerecord.attributes.result_body.string5"),
        I18n.t("activerecord.attributes.result_body.decimal1"),
        I18n.t("activerecord.attributes.result_body.decimal2"),
        I18n.t("activerecord.attributes.result_body.decimal3"),
        I18n.t("activerecord.attributes.result_body.decimal4"),
        I18n.t("activerecord.attributes.result_body.decimal5"),
        I18n.t("activerecord.attributes.result_body.date1"),
        I18n.t("activerecord.attributes.result_body.date2"),
        I18n.t("activerecord.attributes.result_body.date3"),
        I18n.t("activerecord.attributes.result_body.date4"),
        I18n.t("activerecord.attributes.result_body.date5")
      ]

      results = ResultHead.search_results(user, params)

      results.each do |r|
        r.result_bodies.each do |b|

          category_name   = r.category.nil? ? "" : r.category.name
          office_name     = r.office.nil?   ? "" : r.office.name
          staff_name      = r.staff.nil?    ? "" : r.staff.name
          item_jancd      = b.item.nil?     ? "" : b.item.jancd
          item_name       = b.item.nil?     ? "" : b.item.name
          item_kind       = b.item.nil?     ? "" : b.item.kind
          item_price      = b.item.nil?     ? "" : b.item.price
          item_carton     = b.item.nil?     ? "" : b.item.carton

          head_date1 = r.date1.blank? ? "" : r.date1.strftime('%Y%m%d')
          head_date2 = r.date2.blank? ? "" : r.date2.strftime('%Y%m%d')
          head_date3 = r.date3.blank? ? "" : r.date3.strftime('%Y%m%d')
          head_date4 = r.date4.blank? ? "" : r.date4.strftime('%Y%m%d')
          head_date5 = r.date5.blank? ? "" : r.date5.strftime('%Y%m%d')
          body_date1 = b.date1.blank? ? "" : b.date1.strftime('%Y%m%d')
          body_date2 = b.date2.blank? ? "" : b.date2.strftime('%Y%m%d')
          body_date3 = b.date3.blank? ? "" : b.date3.strftime('%Y%m%d')
          body_date4 = b.date4.blank? ? "" : b.date4.strftime('%Y%m%d')
          body_date5 = b.date5.blank? ? "" : b.date5.strftime('%Y%m%d')

          csv << [
            r.result_date.strftime('%Y%m%d'),
            b.result_time.strftime('%H%M%S'),
            r.categorycd,
            category_name,
            r.officecd,
            office_name,
            r.staffcd,
            staff_name,
            r.jobno,
            r.string1,
            r.string2,
            r.string3,
            r.string4,
            r.string5,
            r.decimal1,
            r.decimal2,
            r.decimal3,
            r.decimal4,
            r.decimal5,
            head_date1,
            head_date2,
            head_date3,
            head_date4,
            head_date5,
            b.row,
            b.skucd,
            item_jancd,
            item_name,
            item_kind,
            item_price,
            item_carton,
            b.quantity,
            b.string1,
            b.string2,
            b.string3,
            b.string4,
            b.string5,
            b.decimal1,
            b.decimal2,
            b.decimal3,
            b.decimal4,
            b.decimal5,
            body_date1,
            body_date2,
            body_date3,
            body_date4,
            body_date5,
          ]
        end

        # CSV出力済フラグ更新
        r.export = true
        r.save(validate: false)

      end
    end

    csv_data = csv_data.tosjis
    return csv_data
  end

end