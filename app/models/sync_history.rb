class SyncHistory < ActiveRecord::Base
  # ===============
  # Mass Assignment
  # ===============
  attr_accessible :user_id, :cd

  # ===========
  # Association
  # ===========
  belongs_to  :user

  # =========
  # Validates
  # =========
  validates :cd,
    presence: true,
    uniqueness: { scope: :user_id }

  # =====
  # Scope
  # =====
  scope :user_sync_histories,  lambda { |user| where('user_id = ?', user.id) }
  scope :order_by_id,   lambda { order('id') }

  # ===============
  # Instance Method
  # ===============

  # ============
  # Class Method
  # ============
end
