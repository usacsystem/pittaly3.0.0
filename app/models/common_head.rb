# coding: utf-8
class CommonHead < ActiveRecord::Base
  # ===============
  # Mass Assignment
  # ===============
  attr_accessible :user_id, :cd, :name, :common_bodies_attributes

  # ===========
  # Association
  # ===========
  belongs_to :user
  has_many   :common_bodies,  dependent: :delete_all
  accepts_nested_attributes_for :common_bodies, allow_destroy: true

  # =========
  # Validates
  # =========
  validates :cd,    presence: true,   uniqueness: { scope: :user_id }
  validates :name,  presence: true
  validates_associated :common_bodies

  # =====
  # Scope
  # =====
  scope :user_common_heads,     lambda { |user| where('user_id = ?', user.id) }
  scope :order_by_id,           lambda { order('id') }

  # ============
  # Class Method
  # ============
  def self.user_common_head(user, id)
    where('user_id = ? and id = ?', user.id, id).first
  end

  def self.user_new(user)
    new({user_id: user.id})
  end

  def self.user_common_head_create(user, params)
    new({
      user_id:                  user.id,
      cd:                       params[:cd],
      name:                     params[:name],
      common_bodies_attributes: params[:common_bodies_attributes]
    })
  end

  def self.user_common_head_import_from_csv(user, row)
    new({
      user_id:                  user.id,
      cd:                       row[0],
      name:                     row[1]
    })
  end
end
