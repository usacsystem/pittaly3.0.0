# coding: utf-8
class Category < ActiveRecord::Base
  # ===============
  # Mass Assignment
  # ===============
  attr_accessible :user_id,
                  :cd,
                  :name,
                  :head_strings_attributes,
                  :head_decimals_attributes,
                  :head_dates_attributes,
                  :body_strings_attributes,
                  :body_decimals_attributes,
                  :body_dates_attributes

  # ===========
  # Association
  # ===========
  belongs_to  :user
  has_many    :result_heads
  has_many    :preparations
  has_many    :head_strings,    dependent: :destroy
  has_many    :head_decimals,   dependent: :destroy
  has_many    :head_dates,      dependent: :destroy
  has_many    :body_strings,    dependent: :destroy
  has_many    :body_decimals,   dependent: :destroy
  has_many    :body_dates,      dependent: :destroy
  accepts_nested_attributes_for   :head_strings,  allow_destroy: true
  accepts_nested_attributes_for   :head_decimals, allow_destroy: true
  accepts_nested_attributes_for   :head_dates,    allow_destroy: true
  accepts_nested_attributes_for   :body_strings,  allow_destroy: true
  accepts_nested_attributes_for   :body_decimals, allow_destroy: true
  accepts_nested_attributes_for   :body_dates,    allow_destroy: true

  # =========
  # Validates
  # =========
  validates :cd,    presence: true,   uniqueness: { scope: :user_id }
  validates :name,  presence: true

  # =====
  # Scope
  # =====
  scope :user_categories,  lambda { |user| where('user_id = ?', user.id) }
  scope :order_by_id,   lambda { order('id') }

  # ============
  # Class Method
  # ============
  def self.user_category(user, id)
    where('user_id = ? and id = ?', user.id, id).first
  end

  def self.user_new(user)
    new({user_id: user.id})
  end

  # def self.user_category_create(user, params)
  #   new({
  #     user_id:                  user.id,
  #     cd:                       params[:cd],
  #     name:                     params[:name]
  #   })
  # end
end
