# coding: utf-8
class Device < ActiveRecord::Base
  # ===============
  # Mass Assignment
  # ===============
  attr_accessible :license_id, :name, :architecture, :density, :dpi, :height, :width,
                  :locale, :model, :platform, :osname, :ostype, :processor, :version, :appversion

  # ===========
  # Association
  # ===========
  belongs_to :license

  # =========
  # Validates
  # =========
  validates :license_id, uniqueness: true

  # =====
  # Scope
  # =====

  # ============
  # Class Method
  # ============
  def self.license_device_create(license, params)
    new({
      license_id:     license.id,
      name:           params['name'],
      architecture:   params['architecture'],
      density:        params['density'],
      dpi:            params['dpi'],
      height:         params['height'],
      width:          params['width'],
      locale:         params['locale'],
      model:          params['model'],
      platform:       params['platform'],
      osname:         params['osname'],
      ostype:         params['ostype'],
      processor:      params['processor'],
      version:        params['version'],
      appversion:     params['appversion']
    })
  end

end
