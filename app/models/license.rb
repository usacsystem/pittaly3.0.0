# coding: utf-8
class License < ActiveRecord::Base

  require 'bcrypt'

  # ===============
  # Mass Assignment
  # ===============
  attr_accessible :user_id, :name, :account, :uuid,
                  :password, :password_confirmation, :activation
  attr_accessor   :password
  before_save     :encrypt_password

  # ===========
  # Association
  # ===========
  belongs_to  :user
  has_one     :device,  dependent: :destroy

  # =========
  # Validates
  # =========
  validates :name,      presence: true
  validates :account,   presence: true,   uniqueness: { scope: [:user_id, :uuid] }
  validates :password,  presence: true,   length: {within: 6..20}, confirmation: true
  validates :password_confirmation, presence: true
  validates :uuid,      presence: true

  # =====
  # Scope
  # =====
  scope :user_licenses,             lambda { |user| where('user_id = ?', user.id) }
  scope :order_by_id,               lambda { order('id') }
  scope :activate_licenses,         lambda { |boolean| where('activation = ?', boolean) }

  # ============
  # Class Method
  # ============
  def self.user_license(user, id)
    where('user_id = ? and id = ?', user.id, id).first
  end

  def self.user_license_by_uuid(user, uuid)
    where('user_id = ? and uuid = ?', user.id, uuid).activate_licenses(true).first
  end

  def self.user_license_count_by_uuid(user, uuid)
    where('user_id = ? and uuid = ?', user.id, uuid).activate_licenses(true).count
  end

  def self.user_new(user)
    new({user_id: user.id})
  end

  def self.user_license_create(user, params)
    new({
      user_id:                user.id,
      name:                   params[:name],
      account:                params[:account],
      password:               params[:password],
      password_confirmation:  params[:password_confirmation],
      uuid:                   params[:uuid]
    })
  end

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

  def self.activate_license(user, id, boolean)
    license = self.user_license(user, id)
    license.activation = boolean
    if license.save!(validate: false)
      if boolean
        { update_activate_license: true, activation: true }
      else
        { update_activate_license: true, activation: false }
      end
    else
      { update_activate_license: false }
    end
  end

  def self.default_license_count
    count = 0
    if self.count <= 50
      count = self.count
    else
      count = 50
    end
    return count
  end

  def self.option_license_count
    count = 0
    if self.count > 50
      count = self.count - 50
    else
      count = 0
    end
    return count
  end

  def self.default_license_amount
    default_license_count * DEFAULT_UNIT_PRICE
  end

  def self.option_license_amount
    option_license_count * OPTION_UNIT_PRICE
  end

end