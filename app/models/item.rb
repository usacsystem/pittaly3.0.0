# coding: utf-8
class Item < ActiveRecord::Base
  # ===============
  # Mass Assignment
  # ===============
  attr_accessible :user_id, :skucd, :jancd, :name, :kind, :price, :carton

  # ===========
  # Association
  # ===========
  belongs_to  :user
  has_many    :common_bodies
  has_many    :result_bodies

  # =========
  # Validates
  # =========
  validates   :skucd,   presence: true,   uniqueness: { scope: :user_id }
  validates   :jancd,   presence: true,   uniqueness: { scope: :user_id }
  validates   :name,    presence: true
  validates   :price,   presence: true
  validates   :carton,  presence: true

  # =====
  # Scope
  # =====
  scope :user_items,          lambda { |user| where('user_id = ?', user.id) }
  scope :order_by_id,         lambda { order('id') }

  # ============
  # Class Method
  # ============
  def self.user_item(user, id)
    where('user_id = ? and id = ?', user.id, id).first
  end

  def self.user_item_by_skucd(user, skucd)
    where('user_id = ? and skucd = ?', user.id, skucd).first
  end

  def self.user_item_by_jancd(user, jancd)
    where('user_id = ? and jancd = ?', user.id, jancd).first
  end

  def self.user_new(user)
    new({user_id: user.id})
  end

  def self.user_item_create(user, params)
    new({
      user_id:  user.id,
      skucd:    params[:skucd],
      jancd:    params[:jancd],
      name:     params[:name],
      kind:     params[:kind],
      price:    params[:price],
      carton:   params[:carton]
    })
  end

  def self.user_item_import_from_csv(user, row)
    new({
      user_id:  user.id,
      skucd:    row[0],
      jancd:    row[1],
      name:     row[2],
      kind:     row[3],
      price:    row[4],
      carton:   row[5]
    })
  end
end
