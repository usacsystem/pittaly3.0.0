class Profile < ActiveRecord::Base
  attr_accessible :user_id, :person, :company, :zipcd, :state, :address, :building, :telno, :faxno

  belongs_to :user

  validates :person,  presence: true
  validates :company, presence: true
end
