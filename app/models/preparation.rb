class Preparation < ActiveRecord::Base
  attr_accessible :category_id,
                  :kind,
                  :type,
                  :hint

  belongs_to :category

  # =====
  # Scope
  # =====
  scope :order_by_id,       lambda { order('id') }
end
