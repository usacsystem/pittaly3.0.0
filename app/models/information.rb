# coding: utf-8

class Information < ActiveRecord::Base
  attr_accessible :important_kbn,
                  :title,
                  :article

  # =====
  # Scope
  # =====
  # クラスメソッドで日付を扱う時は要注意(ロケール？)
  scope :current_article,   lambda {where('updated_at >= ?', 30.days.ago) }
  scope :order_by_id_desc,  lambda {order("id DESC")}

end
