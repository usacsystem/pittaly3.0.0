# coding: utf-8
class CommonBody < ActiveRecord::Base
  # ===============
  # Mass Assignment
  # ===============
  attr_accessible :common_head_id, :skucd

  # ===========
  # Association
  # ===========
  belongs_to :common_head
  # ユーザIDを条件に含める
  belongs_to :item, foreign_key: :skucd, primary_key: :skucd, conditions: proc { ['user_id = ?', self.common_head.user_id] }

  # =========
  # Validates
  # =========
  validates :skucd, presence: true

  # =====
  # Scope
  # =====

  # ============
  # Class Method
  # ============
  def self.user_common_body_import_from_csv(common_head, row)
    new({
      common_head_id:           common_head.id,
      skucd:                    row[2]
    })
  end

end
