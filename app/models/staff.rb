# coding: utf-8
class Staff < ActiveRecord::Base
  # ===============
  # Mass Assignment
  # ===============
  attr_accessible :user_id, :cd, :name, :office_cd

  # ===========
  # Association
  # ===========
  belongs_to  :user
  has_many    :result_heads

  # =========
  # Validates
  # =========
  validates :cd,          presence: true,   uniqueness: { scope: :user_id }
  validates :name,        presence: true

  # =====
  # Scope
  # =====
  scope :user_staffs,   lambda { |user| where('user_id = ?', user.id) }
  scope :order_by_id,   lambda { order('id') }

  # ============
  # Class Method
  # ============
  def self.user_staff(user, id)
    where('user_id = ? and id = ?', user.id, id).first
  end

  def self.user_new(user)
    new({user_id: user.id})
  end

  def self.user_staff_create(user, params)
    new({
      user_id:    user.id,
      cd:         params[:cd],
      name:       params[:name],
      office_cd:  params[:office_cd]
    })
  end

  def self.user_staff_import_from_csv(user, row)
    new({
      user_id:    user.id,
      cd:         row[0],
      name:       row[1],
      office_cd:  row[2]
    })
  end
end
