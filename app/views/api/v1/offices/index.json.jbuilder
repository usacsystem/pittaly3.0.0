json.array! @offices do |office|
  json.cd     office.cd
  json.name   office.name
end