json.array! @items do |item|
  json.skucd  item.skucd
  json.jancd  item.jancd
  json.name   item.name
  json.kind   item.kind
  json.price  item.price
  json.carton item.carton
end