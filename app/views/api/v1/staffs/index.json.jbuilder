json.array! @staffs do |staff|
  json.cd           staff.cd
  json.name         staff.name
  if staff.office_cd.nil?
    json.officecd     0
  else
    json.officecd     staff.office_cd
  end
end
