json.array! @common_heads do |common_head|
  json.cd         common_head.cd
  json.name       common_head.name

  array = []
  common_head.common_bodies.each do |common_body|
    item = {
        skucd:    common_body.item.skucd,
        jancd:    common_body.item.jancd,
        name:     common_body.item.name,
        kind:     common_body.item.kind,
        price:    common_body.item.price,
        carton:   common_body.item.carton
    }
    array.push(item)
  end

  json.items array
end
