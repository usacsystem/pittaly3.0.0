json.array! @categories do |category|
  json.cd     category.cd
  json.name   category.name

  preparations = []
  category.preparations.order_by_id.each do |preparation|
    params = {
      type:     preparation[:type],
      hint:     preparation[:hint]
    }
    preparations.push(params)
  end

  json.preparation  preparations
end
