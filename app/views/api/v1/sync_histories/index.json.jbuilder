json.array! @sync_histories do |history|
  json.cd           history.cd
  json.updated_at   history.updated_at.strftime("%Y-%m-%d %H:%M:%S")
end
